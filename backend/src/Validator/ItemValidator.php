<?php

namespace App\Validator;

use App\Entity\Menu\Item;
use App\Enum\Menu\Item\TypeEnum;
use Exception;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class ItemValidator
{
    /**
     * @throws Exception
     */
    public static function validate(Item $item, ExecutionContextInterface $context, mixed $payload): void
    {
        match ($item->getType()) {
            TypeEnum::external->value => $item->setInternalLinkPath(null),
            TypeEnum::internal->value => $item->setExternalLinkUrl(null),
            null => throw new Exception('Invalid item type'),
            default => throw new Exception('Not supported to item type'),
        };

        if ($item->getExternalLinkUrl() || $item->getInternalLinkPath()) {
            return;
        }

        match ($item->getType()) { // @phpstan-ignore-line
            TypeEnum::external->value => $context->buildViolation('This field must be set.')
                ->atPath(Item::FIELD_EXTERNAL_LINK_URL)
                ->addViolation(),
            TypeEnum::internal->value => $context->buildViolation('This field must be set.')
                ->atPath(Item::FIELD_INTERNAL_LINK_PATH)
                ->addViolation(),
        };
    }
}