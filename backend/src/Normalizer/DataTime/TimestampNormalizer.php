<?php

namespace App\Normalizer\DataTime;

use DateTime;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class TimestampNormalizer implements NormalizerInterface
{
    public const TIMESTAMP_IN_MILLISECONDS = 'in_milliseconds';

    /**
     * {@inheritDoc}
     *
     * @param DateTime $object
     */
    public function normalize(mixed $object, ?string $format = null, array $context = []): array|string|int|float|bool|\ArrayObject|null
    {
        $timestamp = $object->getTimestamp();

        if (in_array(self::TIMESTAMP_IN_MILLISECONDS, $context)) {
            $timestamp = $timestamp * 1000;
        }

        return $timestamp;
    }

    /** {@inheritDoc} */
    public function supportsNormalization(mixed $data, ?string $format = null, array $context = []): bool
    {
        return $data instanceof \DateTimeInterface;
    }

    /** {@inheritDoc} */
    public function getSupportedTypes(?string $format): array
    {
        return [
            \DateTimeInterface::class => true,
            \DateTimeImmutable::class => true,
            DateTime::class => true,
        ];
    }
}