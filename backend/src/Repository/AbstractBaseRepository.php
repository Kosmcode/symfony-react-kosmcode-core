<?php

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
 */
abstract class AbstractBaseRepository extends ServiceEntityRepository implements AbstractBaseRepositoryInterface // @phpstan-ignore-line
{
    /** {@inheritdoc } */
    public function persist(object $entity, bool $andFlush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($andFlush) {
            $this->getEntityManager()->flush();
        }
    }

    /** {@inheritdoc } */
    public function flush(): void
    {
        $this->getEntityManager()->flush();
    }

    /** {@inheritdoc } */
    public function remove(object $entity): void
    {
        $this->getEntityManager()->remove($entity);
    }
}