<?php

namespace App\Repository;

use App\Entity\Page;
use Doctrine\Persistence\ManagerRegistry;

class PageRepository extends AbstractBaseRepository implements PageRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    /** {@inheritdoc} */
    public function getPublishedPageBySlug(string $slug): ?Page
    {
        return $this->findOneBy([Page::FIELD_SLUG => $slug, Page::FIELD_PUBLISHED => true]);
    }

    /** {@inheritdoc} */
    public function getAllPublished(): array
    {
        return $this->findBy([Page::FIELD_PUBLISHED => true]);
    }
}
