<?php

namespace App\Repository\Asset;

use App\Entity\Asset\Image;
use App\Repository\AbstractBaseRepository;
use Doctrine\Persistence\ManagerRegistry;

class ImageRepository extends AbstractBaseRepository implements ImageRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Image::class);
    }
}
