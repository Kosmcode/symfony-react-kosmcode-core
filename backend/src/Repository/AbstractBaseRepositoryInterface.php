<?php

namespace App\Repository;

interface AbstractBaseRepositoryInterface
{
    const ORDER_DESC = 'DESC';
    const ORDER_ASC = 'ASC';

    /**
     * @param object $entity
     *
     * @return void
     */
    public function persist(object $entity): void;

    /**
     * @return void
     */
    public function flush(): void;

    /**
     * @param object $entity
     *
     * @return void
     */
    public function remove(object $entity): void;

    /**
     * @param array<string, string> $criteria
     * @param array<string, string>|null $orderBy
     * @param int|null $limit
     * @param int|null $offset
     *
     * @return array<int, object>
     */
    public function findBy(
        array $criteria,
        array|null $orderBy = null,
        int|null $limit = null,
        int|null $offset = null
    ): array;


    /**
     * @param array<string, string> $criteria
     *
     * @return int
     */
    public function count(array $criteria = []): int;

    /**
     * @return array<mixed>
     */
    public function findAll(): array;

    /**
     * @param array<string, mixed> $criteria
     * @param array<string, string>|null $orderBy
     *
     * @return object|null
     */
    public function findOneBy(array $criteria, array|null $orderBy = null): object|null;
}