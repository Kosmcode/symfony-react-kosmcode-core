<?php

namespace App\Repository;

use ApiPlatform\Doctrine\Orm\Paginator;
use App\Entity\Post;
use Doctrine\ORM\Tools\Pagination\Paginator as DoctrinePaginator;

interface PostRepositoryInterface extends AbstractBaseRepositoryInterface
{
    /**
     * @param string $slug
     *
     * @return Post|null
     */
    public function getPublishedPostBySlug(string $slug): ?Post;

    /**
     * @return array<int, object>
     */
    public function getAllPublishedPosts(): array;
}