<?php

namespace App\Repository;

use App\Entity\Page;

interface PageRepositoryInterface extends AbstractBaseRepositoryInterface
{
    /**
     * @param string $slug
     * @return Page|null
     */
    public function getPublishedPageBySlug(string $slug): ?Page;

    /**
     * @return array<int, object>
     */
    public function getAllPublished(): array;
}
