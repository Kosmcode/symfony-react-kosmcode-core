<?php

namespace App\Repository;

use App\Entity\Post;
use App\Enum\Post\StatusEnum;
use Doctrine\Persistence\ManagerRegistry;

class PostRepository extends AbstractBaseRepository implements PostRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    /** {@inheritDoc} */
    public function getPublishedPostBySlug(string $slug): ?Post
    {
        return $this->findOneBy([
            Post::FIELD_SLUG => $slug,
            Post::FIELD_STATUS => StatusEnum::published->value
        ]);
    }

    /** {@inheritDoc} */
    public function getAllPublishedPosts(): array
    {
        return $this->findBy([
            Post::FIELD_STATUS => StatusEnum::published->value
        ]);
    }
}
