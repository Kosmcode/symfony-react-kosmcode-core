<?php

namespace App\Repository\Menu;

use App\Entity\Menu\Item;
use App\Repository\AbstractBaseRepository;
use Doctrine\Persistence\ManagerRegistry;

class ItemRepository extends AbstractBaseRepository implements ItemRepositoryInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Item::class);
    }

    /** {@inheritdoc } */
    public function findOneByPositionAndNotEqualId(int $position, int $notEqualId): ?Item
    {
        $result = $this->createQueryBuilder('item')
            ->where('item.position = :position')
            ->andWhere('item.id != :notEqualId')
            ->setParameter('position', $position)
            ->setParameter('notEqualId', $notEqualId)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return ($result)
            ? $result[0]
            : null;
    }

    /** {@inheritdoc } */
    public function getLastItemPosition(): ?int
    {
        $result = $this->createQueryBuilder('item')
            ->select('MAX(item.position) AS lastPosition')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        return ($result)
            ? $result[0]['lastPosition']
            : null;
    }
}
