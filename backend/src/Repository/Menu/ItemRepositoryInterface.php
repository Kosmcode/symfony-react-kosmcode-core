<?php

namespace App\Repository\Menu;

use App\Entity\Menu\Item;
use App\Repository\AbstractBaseRepositoryInterface;

interface ItemRepositoryInterface extends AbstractBaseRepositoryInterface
{
    /**
     * @param int $position
     * @param int $notEqualId
     *
     * @return Item|null
     */
    public function findOneByPositionAndNotEqualId(int $position, int $notEqualId): ?Item;

    /**
     * @return int|null
     */
    public function getLastItemPosition(): ?int;
}