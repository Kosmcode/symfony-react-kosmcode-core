<?php

namespace App\Trait;

trait EnumTrait
{
    /**
     * @return array<string, int>
     */
    public static function toEasyAdminChoiceField(): array
    {
        $array = [];

        foreach (self::cases() as $case) {
            $array[ucfirst($case->name)] = $case->value;
        }

        return $array;
    }
}