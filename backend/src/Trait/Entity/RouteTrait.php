<?php

namespace App\Trait\Entity;

use App\Enum\RouteEnum;

trait RouteTrait
{
    public function getFrontendRoute(): string
    {
        return $this->prepareFrontendRoute($this->getSlug() ?? '');
    }

    public function prepareFrontendRoute(string $slug): string
    {
        return str_replace(RouteEnum::SLUG_PREFIX, $slug, $this->routeEnum->value);
    }
}