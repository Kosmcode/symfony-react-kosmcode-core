<?php

namespace App\Entity\Asset;

use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use App\Entity\AbstractEntity;
use App\Entity\Page;
use App\Entity\Post;
use App\Repository\Asset\ImageRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use ApiPlatform\Metadata\Get;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: ImageRepository::class)]
#[ORM\Table(name: 'asset_images')]
#[ApiResource(
    operations: [
        new Get(
            normalizationContext: ['groups' => ['image:read']],
        )
    ],
    graphQlOperations: []
)]
class Image extends AbstractEntity
{
    const FIELD_TITLE = 'title';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[ApiProperty(identifier: false)]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(length: 255)]
    #[ApiProperty(identifier: true)]
    private ?string $slug = null;

    #[ORM\Column(length: 255)]
    private ?string $filename = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Groups(['image:read'])]
    private ?string $base64 = null;

    /**
     * @var Collection<int, Page>|ArrayCollection<int, Page>
     */
    #[ORM\OneToMany(targetEntity: Page::class, mappedBy: 'headerImage')]
    private Collection $pages;

    /**
     * @var Collection<int, Post>|ArrayCollection<int, Post>
     */
    #[ORM\OneToMany(targetEntity: Post::class, mappedBy: 'headerImage')]
    private Collection $posts;

    public function __construct()
    {
        $this->pages = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;

        return $this;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): static
    {
        $this->filename = $filename;

        return $this;
    }

    public function getBase64(): ?string
    {
        return $this->base64;
    }

    public function setBase64(string $base64): static
    {
        $this->base64 = $base64;

        return $this;
    }

    /**
     * @return Collection<int, Page>
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    /**
     * @param Collection<int, Page> $pages
     *
     * @return $this
     */
    public function setPages(Collection $pages): static
    {
        $this->pages = $pages;

        return $this;
    }

    /**
     * @return Collection<int, Post>
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param Collection<int, Post> $posts
     *
     * @return $this
     */
    public function setPosts(Collection $posts): static
    {
        $this->posts = $posts;

        return $this;
    }

    public function __toString(): string
    {
        return (string) $this->getTitle();
    }
}
