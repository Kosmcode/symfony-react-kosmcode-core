<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Query;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping\Column;
use Doctrine\ORM\Mapping\Entity;
use Doctrine\ORM\Mapping\GeneratedValue;
use Doctrine\ORM\Mapping\Id;
use Doctrine\ORM\Mapping\Table;
use Nucleos\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

#[Entity] // @phpstan-ignore-line
#[Table(name: 'users')]
#[ApiResource(
    operations: [],
    graphQlOperations: [
        new Query(),
    ]
)]
class User extends BaseUser
{
    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const FIELD_USERNAME = 'username';

    #[GeneratedValue(strategy: 'AUTO')]
    #[Column(type: 'integer')]
    #[Id]
    private ?int $id = null;

    /**
     * @var Collection<int, Post>|ArrayCollection<int, Post>
     */
    #[ORM\OneToMany(targetEntity: Post::class, mappedBy: 'posts')]
    private Collection $posts;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return Collection<int, Post>
     */
    public function getPosts(): Collection
    {
        return $this->posts;
    }

    /**
     * @param Collection<int, Post> $posts
     * @return $this
     */
    public function setPosts(Collection $posts): static
    {
        $this->posts = $posts;

        return $this;
    }


}
