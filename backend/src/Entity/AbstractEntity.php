<?php

namespace App\Entity;

abstract class AbstractEntity
{
    const FIELD_ID = 'id';
}