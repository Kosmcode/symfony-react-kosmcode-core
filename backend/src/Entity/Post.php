<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Query;
use ApiPlatform\Metadata\GraphQl\QueryCollection;
use App\Entity\Asset\Image;
use App\Enum\RouteEnum;
use App\Normalizer\DataTime\TimestampNormalizer;
use App\Repository\PostRepository;
use App\Resolver\Post\SlugResolver;
use App\Trait\Entity\RouteTrait;
use DateTime;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Context;

#[ORM\Entity(repositoryClass: PostRepository::class)]
#[ORM\Table(name: 'posts')]
#[ApiResource(
    operations: [],
    paginationClientEnabled: true,
    graphQlOperations: [
        new Query(),
        new Query(
            resolver: SlugResolver::class,
            args: [
                'slug' => ['type' => 'String'],
            ],
            name: 'slugQuery',
        ),
        new QueryCollection(
            paginationType: 'page',
            paginationItemsPerPage: 5,
            paginationMaximumItemsPerPage: 20,
            order: ['id' => 'DESC'],
        ),
    ]
)]
class Post extends AbstractEntity
{
    use RouteTrait;
    protected RouteEnum $routeEnum = RouteEnum::post;

    const FIELD_SLUG = 'slug';
    const FIELD_STATUS = 'status';
    const FIELD_TITLE = 'title';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\ManyToOne(targetEntity: Image::class, inversedBy: 'posts')]
    #[ORM\JoinColumn(name: 'header_image_id', referencedColumnName: 'id', unique: false, nullable: true)]
    private ?Image $headerImage = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $content = null;

    #[ORM\Column(type: 'tinyint', nullable: false)]
    private ?int $status = null;

    #[ORM\Column(length: 255, nullable: false)]
    private ?string $headerDescription = null;

    #[ORM\ManyToOne(targetEntity: User::class, inversedBy: 'posts')]
    #[ORM\JoinColumn(name: 'user_id', referencedColumnName: 'id', unique: false, nullable: true)]
    private ?User $user = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column(length: 255)]
    private ?string $metaTitle = null;

    #[ORM\Column(length: 160)]
    private ?string $metaKeywords = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $metaDescription = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(name: 'created_at', type: Types::DATETIME_MUTABLE)]
    #[Context([TimestampNormalizer::TIMESTAMP_IN_MILLISECONDS => true])]
    private ?DateTime $createdAt = null;

    /**
     * @var DateTime|null
     */
    #[ORM\Column(name: 'updated_at', type: Types::DATETIME_MUTABLE)]
    #[Context([TimestampNormalizer::TIMESTAMP_IN_MILLISECONDS => true])]
    private ?DateTime $updatedAt = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(string $metaTitle): static
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(string $metaDescription): static
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(string $metaKeywords): static
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    public function getHeaderImage(): ?Image
    {
        return $this->headerImage;
    }

    public function setHeaderImage(?Image $headerImage): static
    {
        $this->headerImage = $headerImage;

        return $this;
    }

    public function getHeaderDescription(): ?string
    {
        return $this->headerDescription;
    }

    public function setHeaderDescription(?string $headerDescription): static
    {
        $this->headerDescription = $headerDescription;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): static
    {
        $this->status = $status;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): static
    {
        $this->user = $user;

        return $this;
    }

    public function getCreatedAt(): ?DateTime
    {
        return $this->createdAt;
    }

    public function getUpdatedAt(): ?DateTime
    {
        return $this->updatedAt;
    }

    public function setCreatedAt(?DateTime $createdAt): static
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function setUpdatedAt(?DateTime $updatedAt): static
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
