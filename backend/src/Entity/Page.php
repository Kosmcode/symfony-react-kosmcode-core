<?php

namespace App\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\GraphQl\Query;
use App\Entity\Asset\Image;
use App\Enum\RouteEnum;
use App\Repository\PageRepository;
use App\Resolver\Page\SlugResolver;
use App\Trait\Entity\RouteTrait;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PageRepository::class)]
#[ORM\Table(name: 'pages')]
#[ApiResource(
    operations: [],
    graphQlOperations: [
        new Query(),
        new Query(
            resolver: SlugResolver::class,
            args: [
                'slug' => ['type' => 'String'],
            ],
            name: 'slugQuery',
        ),
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['page.slug' => 'exact'])]
class Page extends AbstractEntity
{
    use RouteTrait;
    protected RouteEnum $routeEnum = RouteEnum::page;

    const FIELD_SLUG = 'slug';
    const FIELD_PUBLISHED = 'published';
    const FIELD_TITLE = 'title';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(type: Types::TEXT)]
    private ?string $content = null;

    #[ORM\Column(length: 255)]
    private ?string $slug = null;

    #[ORM\Column(type: Types::BOOLEAN, nullable: false)]
    private bool $published = false;

    #[ORM\Column(length: 255)]
    private ?string $metaTitle = null;

    #[ORM\Column(length: 160)]
    private ?string $metaDescription = null;

    #[ORM\Column(length: 160)]
    private ?string $metaKeywords = null;

    #[ORM\ManyToOne(targetEntity: Image::class, inversedBy: 'pages')]
    #[ORM\JoinColumn(name: 'header_image_id', referencedColumnName: 'id', unique: false)]
    private ?Image $headerImage = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $headerDescription = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): static
    {
        $this->id = $id;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    public function isPublished(): bool
    {
        return $this->published;
    }

    public function setPublished(bool $published): self
    {
        $this->published = $published;

        return $this;
    }

    public function getMetaTitle(): ?string
    {
        return $this->metaTitle;
    }

    public function setMetaTitle(string $metaTitle): static
    {
        $this->metaTitle = $metaTitle;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->metaDescription;
    }

    public function setMetaDescription(string $metaDescription): static
    {
        $this->metaDescription = $metaDescription;

        return $this;
    }

    public function getMetaKeywords(): ?string
    {
        return $this->metaKeywords;
    }

    public function setMetaKeywords(string $metaKeywords): static
    {
        $this->metaKeywords = $metaKeywords;

        return $this;
    }

    public function getHeaderImage(): ?Image
    {
        return $this->headerImage;
    }

    public function setHeaderImage(?Image $headerImage): static
    {
        $this->headerImage = $headerImage;

        return $this;
    }

    public function getHeaderDescription(): ?string
    {
        return $this->headerDescription;
    }

    public function setHeaderDescription(?string $headerDescription): static
    {
        $this->headerDescription = $headerDescription;

        return $this;
    }
}
