<?php

namespace App\Entity\Menu;

use ApiPlatform\Metadata\ApiResource;
use App\Doctrine\Column\Type\TinyIntType;
use App\Entity\AbstractEntity;
use App\Repository\Menu\ItemRepository;
use App\Validator\ItemValidator;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Table;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Metadata\GraphQl\QueryCollection;

#[ORM\Entity(repositoryClass: ItemRepository::class)]
#[Table(name: 'menu_items')]
#[ApiResource(
    operations: [],
    graphQlOperations: [
        new QueryCollection(order: ['position' => 'ASC']),
    ]
)]
#[Assert\Callback([ItemValidator::class, 'validate'])]
class Item extends AbstractEntity
{
    const FIELD_INTERNAL_LINK_PATH = 'internalLinkPath';
    const FIELD_POSITION = 'position';
    const FIELD_TITLE = 'title';
    const FIELD_EXTERNAL_LINK_URL = 'externalLinkUrl';
    const FIELD_TYPE = 'type';

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $title = null;

    #[ORM\Column(
        type: TinyIntType::NAME,
        length: 1,
        nullable: false,
        options: [
            'comment' => 'Enum ID from Menu Item TypeEnum',
        ]
    )]
    private ?int $type = null;

    #[ORM\Column(type: TinyIntType::NAME)]
    private ?int $position = null;

    #[ORM\Column(length: 255, nullable: true)]
    private ?string $internalLinkPath = null;

    #[ORM\Column(length: 255, nullable: true)]
    #[Assert\Url]
    private ?string $externalLinkUrl = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    public function getType(): ?int
    {
        return $this->type;
    }

    public function setType(int $type): static
    {
        $this->type = $type;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): static
    {
        $this->position = $position;

        return $this;
    }

    public function getInternalLinkPath(): ?string
    {
        return $this->internalLinkPath;
    }

    public function setInternalLinkPath(?string $internalLinkPath): static
    {
        $this->internalLinkPath = $internalLinkPath;

        return $this;
    }

    public function getExternalLinkUrl(): ?string
    {
        return $this->externalLinkUrl;
    }

    public function setExternalLinkUrl(?string $externalLinkUrl): static
    {
        $this->externalLinkUrl = $externalLinkUrl;

        return $this;
    }
}
