<?php

namespace App;

use App\Doctrine\Column\Type\TinyIntType;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

/**
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class Kernel extends BaseKernel
{
    use MicroKernelTrait;

    public function boot(): void
    {
        parent::boot();

        $this->addDoctrineTypes();
    }

    private function addDoctrineTypes(): void
    {
        if (! $this->container) {
            return;
        }

        $entityManager = $this->container->get('doctrine.orm.default_entity_manager');

        if (!$entityManager) {
            return;
        }

        $this->addDoctrineTinyIntType($entityManager); // @phpstan-ignore-line
    }

    /**
     * @throws Exception
     */
    private function addDoctrineTinyIntType(EntityManagerInterface $entityManager): void
    {
        if (Type::hasType(TinyIntType::NAME)) {
            return;
        }

        Type::addType(
            TinyIntType::NAME,
            TinyIntType::class
        );

        $entityManager
            ->getConnection()
            ->getDatabasePlatform()
            ->registerDoctrineTypeMapping(
                TinyIntType::DB_TYPE,
                TinyIntType::NAME
            );
    }
}
