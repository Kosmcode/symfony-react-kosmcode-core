<?php

namespace App\EventListener\Entity;

use App\Entity\Menu\Item;
use KosmCODE\EntityEventBundle\Enum\Event\EntityEnum;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use KosmCODE\EntityEventBundle\EventListener\Entity\AbstractNotifier;

#[AsEntityListener(event: EntityEnum::prePersist->value, method: EntityEnum::prePersist->name, entity: Item::class)]
#[AsEntityListener(event: EntityEnum::preUpdate->value, method: EntityEnum::preUpdate->name, entity: Item::class)]
#[AsEntityListener(event: EntityEnum::postUpdate->value, method: EntityEnum::postUpdate->name, entity: Item::class)]
/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
final class MenuItemNotifier extends AbstractNotifier
{
    public function getEntityClassName(): string
    {
        return Item::class;
    }
}