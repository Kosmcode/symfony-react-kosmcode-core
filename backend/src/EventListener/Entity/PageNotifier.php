<?php

namespace App\EventListener\Entity;

use App\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use KosmCODE\EntityEventBundle\Enum\Event\EntityEnum;
use KosmCODE\EntityEventBundle\EventListener\Entity\AbstractNotifier;

#[AsEntityListener(event: EntityEnum::prePersist->value, method: EntityEnum::prePersist->name, entity: Page::class)]
#[AsEntityListener(event: EntityEnum::preUpdate->value, method: EntityEnum::preUpdate->name, entity: Page::class)]
#[AsEntityListener(event: EntityEnum::postUpdate->value, method: EntityEnum::postUpdate->name, entity: Page::class)]
#[AsEntityListener(event: EntityEnum::postRemove->value, method: EntityEnum::postRemove->name, entity: Page::class)]
/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
final class PageNotifier extends AbstractNotifier
{
    public function getEntityClassName(): string
    {
        return Page::class;
    }
}