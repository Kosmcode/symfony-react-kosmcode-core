<?php

namespace App\EventListener\Entity;

use App\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use KosmCODE\EntityEventBundle\Enum\Event\EntityEnum;
use KosmCODE\EntityEventBundle\EventListener\Entity\AbstractNotifier;

#[AsEntityListener(event: EntityEnum::prePersist->value, method: EntityEnum::prePersist->name, entity: Post::class)]
#[AsEntityListener(event: EntityEnum::preUpdate->value, method: EntityEnum::preUpdate->name, entity: Post::class)]
#[AsEntityListener(event: EntityEnum::postUpdate->value, method: EntityEnum::postUpdate->name, entity: Post::class)]
#[AsEntityListener(event: EntityEnum::postRemove->value, method: EntityEnum::postRemove->name, entity: Post::class)]
/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
final class PostNotifier extends AbstractNotifier
{
    public function getEntityClassName(): string
    {
        return Post::class;
    }
}