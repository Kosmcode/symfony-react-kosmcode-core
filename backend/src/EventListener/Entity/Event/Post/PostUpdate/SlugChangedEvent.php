<?php

namespace App\EventListener\Entity\Event\Post\PostUpdate;

use Doctrine\Common\EventArgs;
use App\Entity\Post;
use App\Service\PostServiceInterface;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PostUpdateInterface;

final class SlugChangedEvent implements PostUpdateInterface
{
    public function __construct(
        private readonly PostServiceInterface $postService,
    )
    {
        
    }

    /** {@inheritDoc} */
    public function getSupportedEntityClassName(): string
    {
        return Post::class;
    }

    /** {@inheritDoc} */
    public function getOrder(): int
    {
        return 1;
    }

    /** 
     * {@inheritDoc}
     * 
     * @param Post $entity 
     */
    public function do(object $entity, EventArgs $args, array $entityChangeSet = []): void // @phpstan-ignore-line
    {
        if(!isset($entityChangeSet[Post::FIELD_SLUG])) {
            return;
        }

        $this->postService->postUpdatePageSlugChange(
            $entity,
            $entityChangeSet[Post::FIELD_SLUG][0]
        );
    }
}