<?php

namespace App\EventListener\Entity\Event\Post\Pre;

use Doctrine\Common\EventArgs;
use App\Entity\Post;
use App\Service\PostServiceInterface;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PreUpdateInterface;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PrePersistInterface;

final class PreparePostBeforeEvent implements PreUpdateInterface, PrePersistInterface
{
    public function __construct(
        private readonly PostServiceInterface $postService,
    )
    {
        
    }

    /** {@inheritDoc} */
    public function getSupportedEntityClassName(): string
    {
        return Post::class;
    }

    /** {@inheritDoc} */
    public function getOrder(): int
    {
        return 1;
    }

    /** 
     * {@inheritDoc}
     * 
     * @param Post $entity 
     */
    public function do(object $entity, EventArgs $args, array $entityChangeSet = []): void // @phpstan-ignore-line
    {
        $this->postService->preparePost($entity);
    }
}