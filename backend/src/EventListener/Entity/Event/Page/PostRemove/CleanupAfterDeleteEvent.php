<?php

namespace App\EventListener\Entity\Event\Page\PostRemove;

use Doctrine\Common\EventArgs;
use App\Service\PageServiceInterface;
use App\Entity\Page;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PostRemoveInterface;

final class CleanupAfterDeleteEvent implements PostRemoveInterface
{
    public function __construct(
        private readonly PageServiceInterface $pageService,
    )
    {
        
    }

    /** {@inheritDoc} */

    public function getSupportedEntityClassName(): string
    {
        return Page::class;
    }

    /** {@inheritDoc} */

    public function getOrder(): int
    {
        return 1;
    }

    /** 
     * {@inheritDoc} 
     * 
     * @param Page $entity 
     */
    public function do(object $entity, EventArgs $args, array $entityChangeSet = []): void // @phpstan-ignore-line
    {
        $this->pageService->cleanupAfterDeletePost($entity);
    }
}
