<?php

namespace App\EventListener\Entity\Event\Page\Pre;

use Doctrine\Common\EventArgs;
use App\Entity\Page;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PreUpdateInterface;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PrePersistInterface;
use Symfony\Component\String\Slugger\SluggerInterface;

final class SlugCheckEvent implements PrePersistInterface, PreUpdateInterface
{
    public function __construct(
        private readonly SluggerInterface $slugger,
    )
    {

    }

    /** {@inheritDoc} */
    public function getSupportedEntityClassName(): string
    {
        return Page::class;
    }

    /** {@inheritDoc} */
    public function getOrder(): int
    {
        return 1;
    }

    /** 
     * {@inheritDoc} 
     * 
     * @param Page $entity 
     */
    public function do(object $entity, EventArgs $args, array $entityChangeSet = []): void // @phpstan-ignore-line
    {
        if ($entity->getSlug()) {
            $entity->setSlug(
                $this->slugger->slug(
                    $entity->getSlug()
                )
            );

            return;
        }

        $entity->setSlug(
            $this->slugger->slug(
                $entity->getTitle()
            )
        );
    }
}
