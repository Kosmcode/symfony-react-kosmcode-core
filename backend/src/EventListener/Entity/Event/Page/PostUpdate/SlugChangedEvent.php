<?php

namespace App\EventListener\Entity\Event\Page\PostUpdate;

use Doctrine\Common\EventArgs;
use App\Entity\Page;
use App\Service\PageServiceInterface;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PostUpdateInterface;

final class SlugChangedEvent implements PostUpdateInterface
{
    public function __construct(
        private readonly PageServiceInterface $pageService
    )
    {
        
    }

    /** {@inheritDoc} */
    public function getSupportedEntityClassName(): string
    {
        return Page::class;
    }

    /** {@inheritDoc} */
    public function getOrder(): int
    {
        return 1;
    }

    /** 
     * {@inheritDoc} 
     * 
     * @param Page $entity 
     */
    public function do(object $entity, EventArgs $args, array $entityChangeSet = []): void // @phpstan-ignore-line
    {
        if (! isset($entityChangeSet[Page::FIELD_SLUG])) {
            return;
        }

        $this->pageService->postUpdatePageSlugChange(
            $entity,
            $entityChangeSet[Page::FIELD_SLUG][0]
        );
    }
}