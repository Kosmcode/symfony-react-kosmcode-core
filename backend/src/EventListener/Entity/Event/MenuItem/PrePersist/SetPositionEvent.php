<?php

namespace App\EventListener\Entity\Event\MenuItem\PrePersist;

use App\Entity\Menu\Item;
use Doctrine\Common\EventArgs;
use App\Service\Menu\ItemServiceInterface;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PrePersistInterface;

final class SetPositionEvent implements PrePersistInterface
{
    public function __construct(
        private readonly ItemServiceInterface $menuItemService,
    )
    {
        
    }

    /** {@inheritDoc} */
    public function getSupportedEntityClassName(): string
    {
        return Item::class;
    }

    /** {@inheritDoc} */
    public function getOrder(): int
    {
        return 1;
    }

    /** 
     * {@inheritDoc} 
     * 
     * @param Item $entity 
     */
    public function do(object $entity, EventArgs $args, array $entityChangeSet = []): void // @phpstan-ignore-line
    {
        if ($entity->getPosition()) {
            return;
        }

        $this->menuItemService->setLastPositionItem($entity);
    }
}
