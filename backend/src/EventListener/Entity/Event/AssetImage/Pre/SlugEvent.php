<?php

namespace App\EventListener\Entity\Event\AssetImage\Pre;

use App\Entity\Asset\Image;
use Doctrine\Common\EventArgs;
use Symfony\Component\String\Slugger\SluggerInterface;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PrePersistInterface;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PreUpdateInterface;

final class SlugEvent implements PrePersistInterface, PreUpdateInterface
{
    public function __construct(
        private readonly SluggerInterface $slugger,
    )
    {
        
    }

    /** {@inheritDoc} */
    public function getSupportedEntityClassName(): string
    {
        return Image::class;
    }

    /** {@inheritDoc} */
    public function getOrder(): int
    {
        return 1;
    }

    /** 
     * {@inheritDoc} 
     * 
     * @param Image $entity
     */
    public function do(object $entity, EventArgs $args, array $entityChangeSet = []): void // @phpstan-ignore-line
    {
        if ($entity->getSlug()) {
            return;
        }

        $entity->setSlug(
            $this->slugger->slug((string) $entity->getTitle())
        );
    }
}