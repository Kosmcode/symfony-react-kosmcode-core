<?php

namespace App\EventListener\Entity\Event\AssetImage\Pre;

use App\Entity\Asset\Image;
use Doctrine\Common\EventArgs;
use App\Service\Asset\ImageServiceInterface;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PrePersistInterface;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PreUpdateInterface;

final class ConvertImageToWebpEvent implements PrePersistInterface, PreUpdateInterface
{
    public function __construct(
        private readonly ImageServiceInterface $imageService,
    )
    {
        
    }

    /** {@inheritDoc} */
    public function getSupportedEntityClassName(): string
    {
        return Image::class;
    }

    /** {@inheritDoc} */
    public function getOrder(): int
    {
        return 2;
    }

    /** 
     * {@inheritDoc} 
     * 
     * @param Image $entity
     */
    public function do(object $entity, EventArgs $args, array $entityChangeSet = []): void // @phpstan-ignore-line
    {
        $this->imageService->convertImageToWebpAndPrepareBase64ImageByImage($entity);
    }
}