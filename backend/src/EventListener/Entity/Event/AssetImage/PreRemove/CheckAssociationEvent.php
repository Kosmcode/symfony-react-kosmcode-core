<?php

namespace App\EventListener\Entity\Event\AssetImage\PreRemove;

use App\Entity\Asset\Image;
use Doctrine\Common\EventArgs;
use Exception;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PreRemoveInterface;

final class CheckAssociationEvent implements PreRemoveInterface
{

    /** {@inheritDoc} */
    public function getSupportedEntityClassName(): string
    {
        return Image::class;
    }

    /** {@inheritDoc} */
    public function getOrder(): int
    {
        return 1;
    }

    /** 
     * {@inheritDoc} 
     * 
     * @param Image $entity
     */
    public function do(object $entity, EventArgs $args, array $entityChangeSet = []): void // @phpstan-ignore-line
    {
        if (!$entity->getPages()->count() && !$entity->getPosts()->count()) {
            return;
        }

        throw new Exception('Image must be unlinked from all Pages and Posts');
    }
}