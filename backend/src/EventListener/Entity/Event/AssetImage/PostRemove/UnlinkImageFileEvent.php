<?php

namespace App\EventListener\Entity\Event\AssetImage\PostRemove;

use App\Entity\Asset\Image;
use App\Service\Asset\ImageServiceInterface;
use KosmCODE\EntityEventBundle\EventListener\Entity\Event\PostRemoveInterface;
use Doctrine\Common\EventArgs;

final class UnlinkImageFileEvent implements PostRemoveInterface
{
    public function __construct(
        private readonly ImageServiceInterface $imageService,
    )
    {
        
    }

    /** {@inheritDoc} */
    public function getSupportedEntityClassName(): string
    {
        return Image::class;
    }

    /** {@inheritDoc} */
    public function getOrder(): int
    {
        return 1;
    }

    /** 
     * {@inheritDoc} 
     * 
     * @param Image $entity
     */
    public function do(object $entity, EventArgs $args, array $entityChangeSet = []): void // @phpstan-ignore-line
    {
        $this->imageService->unlinkImageByImage($entity);
    }
}