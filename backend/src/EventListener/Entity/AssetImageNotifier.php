<?php

namespace App\EventListener\Entity;

use App\Entity\Asset\Image;
use Doctrine\Bundle\DoctrineBundle\Attribute\AsEntityListener;
use KosmCODE\EntityEventBundle\Enum\Event\EntityEnum;
use KosmCODE\EntityEventBundle\EventListener\Entity\AbstractNotifier;

#[AsEntityListener(event: EntityEnum::prePersist->value, method: EntityEnum::prePersist->name, entity: Image::class)]
#[AsEntityListener(event: EntityEnum::preUpdate->value, method: EntityEnum::preUpdate->name, entity: Image::class)]
#[AsEntityListener(event: EntityEnum::preRemove->value, method: EntityEnum::preRemove->name, entity: Image::class)]
#[AsEntityListener(event: EntityEnum::postRemove->value, method: EntityEnum::postRemove->name, entity: Image::class)]
/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class AssetImageNotifier extends AbstractNotifier
{
    public function getEntityClassName(): string
    {
        return Image::class;
    }
}
