<?php

namespace App\Enum;

enum DirectionEnum
{
    case up;
    case down;
}
