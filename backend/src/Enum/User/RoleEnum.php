<?php

namespace App\Enum\User;

use App\Entity\User;

enum RoleEnum: string
{
    case user = User::ROLE_DEFAULT;
    case admin = User::ROLE_ADMIN;

    /**
     * @return array<string, string>
     */
    public static function toOptionArray(): array
    {
        $optionArray = [];

        foreach (self::cases() as $case) {
            $optionArray[ucfirst($case->name)] = $case->value;
        }

        return $optionArray;
    }
}
