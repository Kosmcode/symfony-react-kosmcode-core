<?php

namespace App\Enum\Post;

use App\Trait\EnumTrait;

enum StatusEnum: int
{
    use EnumTrait;

    case draft = 0;
    case published = 1;
}
