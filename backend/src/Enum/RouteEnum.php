<?php

namespace App\Enum;

enum RouteEnum: string
{
    public const SLUG_PREFIX = '{slug}';

    case admin = 'admin';
    case home = '/';
    case posts = 'posts';
    case post = 'post/' . self::SLUG_PREFIX;
    case page = '/' . self::SLUG_PREFIX;
}
