<?php

namespace App\Enum\Menu\Item;

use App\Trait\EnumTrait;

enum TypeEnum: int
{
    use EnumTrait;

    case internal = 1;
    case external = 2;
}
