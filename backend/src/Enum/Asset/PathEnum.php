<?php

namespace App\Enum\Asset;

enum PathEnum: string
{
    case uploadsImages = '/uploads/images/';
    case testImages = '/assets/test/images/';

    public function getAbsolutePath(string $absoluteProjectDirectory): string
    {
        return $absoluteProjectDirectory . $this->value;
    }
}
