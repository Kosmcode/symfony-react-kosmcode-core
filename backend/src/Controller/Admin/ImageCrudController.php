<?php

namespace App\Controller\Admin;

use App\Entity\Asset\Image;
use App\Enum\Asset\PathEnum;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class ImageCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Image::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('title'),
            SlugField::new('slug')
                ->setTargetFieldName('slug'),
            ImageField::new('filename', 'Image')
                ->setBasePath(PathEnum::uploadsImages->value)
                ->setUploadDir(PathEnum::uploadsImages->value)
                ->setUploadedFileNamePattern('[contenthash].[extension]'),
        ];
    }
}
