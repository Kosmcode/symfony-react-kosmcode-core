<?php

namespace App\Controller\Admin;

use App\Entity\Menu\Item;
use App\Enum\DirectionEnum;
use App\Enum\Menu\Item\TypeEnum;
use App\Repository\Menu\ItemRepositoryInterface;
use App\Service\Menu\FactoryInterface;
use Doctrine\ORM\EntityManagerInterface;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\HttpFoundation\Response;


/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 * @SuppressWarnings(PHPMD.StaticAccess)
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ItemCrudController extends AbstractCrudController
{
    public function __construct(
        private EntityManagerInterface    $entityManager,
        private readonly FactoryInterface $menuFactory,
        private readonly ItemRepositoryInterface $itemRepository,
    )
    {
    }

    public static function getEntityFqcn(): string
    {
        return Item::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IntegerField::new(Item::FIELD_POSITION)
                ->setDisabled(),
            TextField::new(Item::FIELD_TITLE),
            ChoiceField::new(Item::FIELD_TYPE)
                ->setChoices(TypeEnum::toEasyAdminChoiceField()),
            TextField::new(Item::FIELD_EXTERNAL_LINK_URL)
                ->hideOnIndex(),
            ChoiceField::new(Item::FIELD_INTERNAL_LINK_PATH)
                ->setChoices(
                    $this->menuFactory->getToEasyAdminOptions()
                )
                ->hideOnIndex(),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['position' => 'ASC'])
            ->setFormOptions([])
            ->showEntityActionsInlined();
    }

    public function configureActions(Actions $actions): Actions
    {
        $entityCount = $this->itemRepository->count([]);

        $moveUp = Action::new('moveUp', false, 'fa fa-sort-up')
            ->setHtmlAttributes(['title' => 'Move up'])
            ->linkToCrudAction('moveUp')
            ->displayIf(fn ($entity) => $entity->getPosition() > 1);

        $moveDown = Action::new('moveDown', false, 'fa fa-sort-down')
            ->setHtmlAttributes(['title' => 'Move down'])
            ->linkToCrudAction('moveDown')
            ->displayIf(fn ($entity) => $entity->getPosition() < $entityCount);

        return $actions
            ->add(Crud::PAGE_INDEX, $moveDown)
            ->add(Crud::PAGE_INDEX, $moveUp);
    }

    public function moveUp(AdminContext $context): Response
    {
        return $this->move($context, DirectionEnum::up);
    }

    public function moveDown(AdminContext $context): Response
    {
        return $this->move($context, DirectionEnum::down);
    }

    private function move(AdminContext $context, DirectionEnum $directionEnum): Response
    {
        $object = $context->getEntity()->getInstance();

        $newPosition = match ($directionEnum) {
            DirectionEnum::up => $object->getPosition() - 1,
            DirectionEnum::down => $object->getPosition() + 1,
        };

        $object->setPosition($newPosition);
        $this->entityManager->flush();

        $this->addFlash('success', 'The element has been successfully moved.');

        return $this->redirect($context->getRequest()->headers->get('referer') ?? DIRECTORY_SEPARATOR);
    }
}
