<?php

namespace App\Override\Nucleos\UserBundle\Action;

use App\Enum\RouteEnum;
use App\Service\UserServiceInterface;
use Nucleos\UserBundle\Event\GetResponseLoginEvent;
use Nucleos\UserBundle\Form\Type\LoginFormType;
use Nucleos\UserBundle\NucleosUserEvents;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Twig\Environment;

class LoginAction
{
    public function __construct(
        protected readonly Environment $twig,
        protected readonly EventDispatcherInterface $eventDispatcher,
        protected readonly FormFactoryInterface $formFactory,
        protected readonly RouterInterface $router,
        protected readonly CsrfTokenManagerInterface $csrfTokenManager,
        protected readonly AuthenticationUtils $authenticationUtils,
        protected readonly UserServiceInterface $userService,
    ) {
    }

    /**
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function __invoke(Request $request): Response
    {
        if ($this->userService->userIsLogged()) {
            return new RedirectResponse($this->router->generate(RouteEnum::admin->value));
        }

        $event = new GetResponseLoginEvent($request);
        $this->eventDispatcher->dispatch($event, NucleosUserEvents::SECURITY_LOGIN_INITIALIZE);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->formFactory
            ->create(LoginFormType::class, null, [
                'action' => $this->router->generate('nucleos_user_security_check'),
                'method' => 'POST',
            ])
            ->add('save', SubmitType::class, [
                'label' => 'security.login.submit',
            ])
        ;

        return new Response($this->twig->render('@NucleosUser/Security/login.html.twig', [
            'form' => $form->createView(),
            'last_username' => $this->authenticationUtils->getLastUsername(),
            'error' => $this->authenticationUtils->getLastAuthenticationError(),
            'csrf_token' => $this->csrfTokenManager->getToken('authenticate'),
        ]));
    }
}
