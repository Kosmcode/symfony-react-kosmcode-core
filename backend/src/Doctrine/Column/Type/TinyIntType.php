<?php

namespace App\Doctrine\Column\Type;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

class TinyIntType extends Type
{
    const NAME = 'tinyint';
    const DB_TYPE = 'TINYINT';

    /** {@inheritdoc} */
    public function getSQLDeclaration(array $column, AbstractPlatform $platform): string
    {
        return self::DB_TYPE;
    }

    /** {@inheritdoc} */
    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        return $value;
    }

    /** {@inheritdoc} */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        return $value;
    }

    /** {@inheritdoc} */
    public function getName(): string
    {
        return self::NAME;
    }
}