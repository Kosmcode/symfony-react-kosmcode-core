<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Faker\Factory;
use Faker\Generator;

/**
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
abstract class AbstractFixture extends Fixture implements OrderedFixtureInterface
{
    protected Generator $faker;

    public function __construct(
    ) {
        $this->faker = Factory::create();
    }

    protected function prepareHtmlParagraphs(int $countParagraphs): string
    {
        $paragraphs = $this->faker->paragraphs($countParagraphs);

        if (is_string($paragraphs)) {
            return '<p>' . $paragraphs . '</p>';
        }

        return '<p>' . join('</p><p>', $paragraphs) . '</p>';
    }

}