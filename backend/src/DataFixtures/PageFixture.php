<?php

namespace App\DataFixtures;

use App\Entity\Page;
use App\Repository\Asset\ImageRepositoryInterface;
use App\Repository\PageRepositoryInterface;
use Doctrine\Persistence\ObjectManager;
use Random\RandomException;

class PageFixture extends AbstractFixture
{
    private const TEST_PAGE_COUNT = 6;
    private const TEST_PAGE_STATUS_NOT_PUBLISHED_MODULO = 3;
    private const TEST_PAGE_TITLE_PATTERN = 'Test page %d';
    private const TEST_PAGE_SLUG_PATTERN = 'test-page-%d';

    public function __construct(
        private readonly PageRepositoryInterface  $pageRepository,
        private readonly ImageRepositoryInterface $imageRepository,
    )
    {
        parent::__construct();
    }

    public function getOrder(): int
    {
        return 3;
    }

    /**
     * @throws RandomException
     */
    public function load(ObjectManager $manager): void
    {
        $images = $this->imageRepository->findAll();

        for ($i = 1; $i < self::TEST_PAGE_COUNT; $i++) {
            $pageTitle = $this->preparePageTitleByCounter($i);

            $page = $this->pageRepository->findBy([Page::FIELD_TITLE => $pageTitle]);

            if ($page) {
                continue;
            }

            $page = (new Page())
                ->setTitle($pageTitle)
                ->setSlug($this->preparePageSlugByCounter($i))
                ->setHeaderDescription($pageTitle)
                ->setMetaDescription($pageTitle)
                ->setMetaTitle($pageTitle)
                ->setMetaKeywords(str_replace(' ', ',', $pageTitle))
                ->setHeaderImage($images[array_rand($images)])
                ->setPublished(
                    ($i % self::TEST_PAGE_STATUS_NOT_PUBLISHED_MODULO === 1)
                )
                ->setContent($this->prepareHtmlParagraphs(random_int(3, 7)));

            $manager->persist($page);
        }

        $manager->flush();
    }

    private function preparePageTitleByCounter(int $counter): string
    {
        return sprintf(
            self::TEST_PAGE_TITLE_PATTERN,
            $counter
        );
    }

    private function preparePageSlugByCounter(int $counter): string
    {
        return sprintf(
            self::TEST_PAGE_SLUG_PATTERN,
            $counter
        );
    }
}
