<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Repository\UserRepositoryInterface;
use Doctrine\Persistence\ObjectManager;

class UserFixture extends AbstractFixture
{
    private const TEST_USERS_COUNT = 5;

    public function __construct(
        private readonly UserRepositoryInterface $userRepository,
    )
    {
        parent::__construct();
    }

    public function getOrder(): int
    {
        return 2;
    }

    public function load(ObjectManager $manager): void
    {
        for($i = 1; $i <= self::TEST_USERS_COUNT; $i++) {
            $username = $this->prepareUsername($i);

            $user = $this->userRepository->findOneBy([User::FIELD_USERNAME => $username]);

            if ($user) {
                continue;
            }

            $user = new User();
            $user->setUsername($username);
            $user->setEmail($this->faker->email);
            $user->setEnabled(true);
            $user->setRoles(['ROLE_USER']);
            $user->setPlainPassword($this->faker->password);

            $manager->persist(
                $user
            );
        }

        $manager->flush();
    }

    private function prepareUsername(int $counter): string
    {
        return sprintf(
            'testuser%d',
            $counter
        );
    }
}
