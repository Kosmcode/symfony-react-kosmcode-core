<?php

namespace App\DataFixtures;

use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

/**
 * @SuppressWarnings(PHPMD.UnusedFormalParameter)
 */
class AppFixtures extends AbstractFixture
{
    public function getOrder(): int
    {
        return 1;
    }

    public function load(ObjectManager $manager): void
    {
        return;
    }
}