<?php

namespace App\DataFixtures;

use App\Entity\Menu\Item;
use App\Entity\Page;
use App\Enum\Menu\Item\TypeEnum;
use App\Enum\RouteEnum;
use App\Repository\Menu\ItemRepositoryInterface;
use App\Repository\PageRepositoryInterface;
use App\Repository\PostRepositoryInterface;
use Doctrine\Persistence\ObjectManager;

class MenuItemFixture extends AbstractFixture
{
    private const HOME_PAGE_TITLE = 'Home';
    private const HOME_PAGE_POSTS = 'Posts';
    private const MENU_ITEMS_EXTERNAL_LINKS = [
        'KosmCODE' => 'https://kosmcode.pl',
    ];

    public function __construct(
        private readonly ItemRepositoryInterface $itemRepository,
        private readonly PageRepositoryInterface $pageRepository,
        private readonly PostRepositoryInterface $postRepository,
    )
    {
        parent::__construct();
    }

    public function getOrder(): int
    {
        return 4;
    }

    public function load(ObjectManager $manager): void
    {
        $this->createMenuItemHome($manager);
        $this->createMenuItemPosts($manager);
        $this->createMenuItemPublishedPages($manager);
        $this->createMenuItemExternalLinks($manager);
    }

    private function createMenuItemHome(ObjectManager $manager): void
    {
        $itemHomePage = $this->itemRepository->findOneBy([Item::FIELD_TITLE => self::HOME_PAGE_TITLE]);

        if ($itemHomePage) {
            return;
        }

        $manager->persist(
            (new Item())
                ->setTitle(self::HOME_PAGE_TITLE)
                ->setPosition(1)
                ->setInternalLinkPath(RouteEnum::home->value)
                ->setType(TypeEnum::internal->value)
        );

        $manager->flush();
    }

    private function createMenuItemPosts(ObjectManager $manager): void
    {
        $itemHomePage = $this->postRepository->findOneBy([Item::FIELD_TITLE => self::HOME_PAGE_POSTS]);

        if ($itemHomePage) {
            return;
        }

        $manager->persist(
            (new Item())
                ->setTitle(self::HOME_PAGE_POSTS)
                ->setPosition(2)
                ->setInternalLinkPath(RouteEnum::posts->value)
                ->setType(TypeEnum::internal->value)
        );

        $manager->flush();
    }

    private function createMenuItemPublishedPages(ObjectManager $manager): void
    {
        $publishedPages = $this->pageRepository->getAllPublished();

        /** @var Page $publishedPage */
        foreach ($publishedPages as $publishedPage) {
            $menuItemPublishedPage = $this->itemRepository->findOneBy([
                Item::FIELD_INTERNAL_LINK_PATH => $publishedPage->getSlug()
            ]);

            if ($menuItemPublishedPage) {
                continue;
            }

            $manager->persist(
                (new Item())
                    ->setTitle((string) $publishedPage->getTitle())
                    ->setInternalLinkPath($publishedPage->getSlug())
                    ->setType(TypeEnum::internal->value)
            );

            $manager->flush();
        }
    }

    private function createMenuItemExternalLinks(ObjectManager $manager): void
    {
        foreach (self::MENU_ITEMS_EXTERNAL_LINKS as $externalTitle => $externalLink) {
            $menuItemPublishedPage = $this->itemRepository->findOneBy([
                Item::FIELD_EXTERNAL_LINK_URL => $externalLink
            ]);

            if ($menuItemPublishedPage) {
                continue;
            }

            $manager->persist(
                (new Item())
                    ->setTitle($externalTitle)
                    ->setExternalLinkUrl($externalLink)
                    ->setType(TypeEnum::external->value)
            );

            $manager->flush();
        }
    }
}
