<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Enum\Post\StatusEnum;
use App\Repository\Asset\ImageRepositoryInterface;
use App\Repository\PostRepositoryInterface;
use App\Repository\UserRepositoryInterface;
use Doctrine\Persistence\ObjectManager;

class PostFixture extends AbstractFixture
{
    private const TEST_POSTS_COUNT = 33;
    private const TEST_POST_STATUS_DRAFT_COUNTER_MODULO = 3;
    private const TEST_POST_TITLE_PATTERN = 'Test Post %d';

    public function __construct(
        private readonly UserRepositoryInterface $userRepository,
        private readonly ImageRepositoryInterface $imageRepository,
        private readonly PostRepositoryInterface $postRepository,
    )
    {
        parent::__construct();
    }

    public function getOrder(): int
    {
        return 3;
    }

    public function load(ObjectManager $manager): void
    {
        $users = $this->userRepository->findAll();
        $images = $this->imageRepository->findAll();

        for ($i = 0; $i < self::TEST_POSTS_COUNT; $i++) {
            $postTitle = $this->prepareTitle($i);

            $post = $this->postRepository->findOneBy([Post::FIELD_TITLE => $postTitle]);

            if ($post) {
                continue;
            }

            $manager->persist(
                (new Post())
                    ->setTitle($postTitle)
                    ->setUser(
                        $users[array_rand($users)]
                    )
                    ->setHeaderImage(
                        $images[array_rand($images)]
                    )
                    ->setHeaderDescription($postTitle)
                    ->setContent($this->prepareHtmlParagraphs(random_int(3,7)))
                    ->setMetaTitle($postTitle)
                    ->setMetaDescription($postTitle)
                    ->setMetaKeywords(str_replace(' ', ', ', $postTitle))
                    ->setStatus(
                        ($i % self::TEST_POST_STATUS_DRAFT_COUNTER_MODULO === 0)
                            ? StatusEnum::draft->value
                            : StatusEnum::published->value
                    )
            );
        }

        $manager->flush();
    }

    private function prepareTitle(int $counter): string
    {
        return sprintf(
            self::TEST_POST_TITLE_PATTERN,
            $counter
        );
    }
}
