<?php

namespace App\DataFixtures;

use App\Entity\Asset\Image;
use App\Enum\Asset\PathEnum;
use App\Repository\Asset\ImageRepositoryInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Filesystem\Filesystem;

class AssetImageFixture extends AbstractFixture
{
    private const TEST_IMAGES_FILENAMES = [
        1 => 'test-image-1.jpg',
        2 => 'test-image-2.jpg',
        3 => 'test-image-3.jpg',
    ];

    private const TEST_IMAGE_TITLE_PATTERN = 'Test Fixture Image %d';
    private const TEST_IMAGE_SLUG_PATTERN = 'test-fixture-image-%d';

    public function __construct(
        private readonly ImageRepositoryInterface $imageRepository,
        private readonly KernelInterface $kernel,
        private readonly Filesystem $filesystem,
    )
    {
        parent::__construct();
    }

    public function getOrder(): int
    {
        return 2;
    }

    public function load(ObjectManager $manager): void
    {
        $projectDir = $this->kernel->getProjectDir();

        foreach (self::TEST_IMAGES_FILENAMES as $imageNumber => $filename) {

            $imageTitle = $this->prepareTitle($imageNumber);

            $image = $this->imageRepository->findBy([Image::FIELD_TITLE => $imageTitle]);

            if ($image) {
                continue;
            }

            $this->filesystem->copy(
                PathEnum::testImages->getAbsolutePath($projectDir) . $filename,
                PathEnum::uploadsImages->getAbsolutePath($projectDir) . $filename
            );

            $manager->persist(
                (new Image())
                    ->setTitle($imageTitle)
                    ->setSlug($this->prepareSlug($imageNumber))
                    ->setFilename($filename)
            );
        }

        $manager->flush();
    }

    private function prepareTitle(int $counter): string
    {
        return sprintf(
            self::TEST_IMAGE_TITLE_PATTERN,
            $counter
        );
    }

    private function prepareSlug(int $counter): string
    {
        return sprintf(
            self::TEST_IMAGE_SLUG_PATTERN,
            $counter
        );
    }
}
