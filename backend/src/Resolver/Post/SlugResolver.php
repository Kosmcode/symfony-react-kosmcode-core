<?php

namespace App\Resolver\Post;

use ApiPlatform\GraphQl\Resolver\QueryItemResolverInterface;
use ApiPlatform\ParameterValidator\Exception\ValidationException;
use App\Entity\Post;
use App\Repository\PostRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class SlugResolver implements QueryItemResolverInterface
{
    public function __construct(
        private readonly PostRepositoryInterface $postRepository,
    )
    {
    }

    /**
     * @param array<mixed> $context
     *
     * @return Post
     *
     * @throws ValidationException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __invoke(?object $item, array $context): object
    {
        if (empty($context['args']['slug'])) {
            throw new ValidationException(['Slug cannot be empty']);
        }

        $post = $this->postRepository->getPublishedPostBySlug($context['args']['slug']);

        if (!$post) {
            throw new NotFoundHttpException('Post not found');
        }

        return $post;
    }
}
