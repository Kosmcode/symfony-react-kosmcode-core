<?php

namespace App\Resolver\Page;

use ApiPlatform\GraphQl\Resolver\QueryItemResolverInterface;
use ApiPlatform\ParameterValidator\Exception\ValidationException;
use App\Entity\Page;
use App\Repository\PageRepositoryInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

final class SlugResolver implements QueryItemResolverInterface
{
    public function __construct(
        private readonly PageRepositoryInterface $pageRepository,
    ) {
    }

    /**
     * @param array<mixed> $context
     *
     * @return Page
     *
     * @throws ValidationException
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function __invoke(?object $item, array $context): object
    {
        if (empty($context['args']['slug'])) {
            throw new ValidationException(['Slug cannot be empty']);
        }

        $page = $this->pageRepository->getPublishedPageBySlug($context['args']['slug']);

        if (!$page) {
            throw new NotFoundHttpException('Page not found');
        }

        return $page;
    }
}
