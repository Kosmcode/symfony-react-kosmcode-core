<?php

namespace App\Service;

use App\Entity\Post;
use App\Service\Menu\ItemServiceInterface;
use DateTime;
use Symfony\Component\String\Slugger\SluggerInterface;

readonly class PostService implements PostServiceInterface
{
    public function __construct(
        protected UserServiceInterface $userService,
        protected SluggerInterface     $slugger,
        protected ItemServiceInterface $menuItemService,
    )
    {

    }

    /** {@inheritdoc} */
    public function preparePost(Post $post): void
    {
        $this->prepareUser($post);
        $this->prepareSlug($post);
        $this->prepareTimestamps($post);
    }

    /** {@inheritdoc} */
    public function cleanupAfterDeletePost(Post $post): void
    {
        $this->menuItemService->deleteAllByInternalLinkPath($post->getFrontendRoute());
    }

    /** {@inheritdoc} */
    public function postUpdatePageSlugChange(Post $post, string $oldSlug): void
    {
        $this->menuItemService->updateInternalLinkRouteByPost($post, $oldSlug);
    }

    /**
     * @param Post $post
     *
     * @return void
     */
    protected function prepareUser(Post $post): void
    {
        if ($post->getUser()) {
            return;
        }

        $post->setUser($this->userService->getUser()); // @phpstan-ignore-line
    }

    /**
     * @param Post $post
     *
     * @return void
     */
    protected function prepareSlug(Post $post): void
    {
        if (empty($post->getSlug())) {
            $post->setSlug(
                $this->slugger->slug($post->getTitle()) // @phpstan-ignore-line
            );

            return;
        }

        $post->setSlug(
            $this->slugger->slug($post->getSlug())
        );
    }

    /**
     * @param Post $post
     *
     * @return void
     */
    protected function prepareTimestamps(Post $post): void
    {
        $timeNow = new DateTime();

        if (!$post->getId()) {
            $post->setCreatedAt($timeNow);
        }

        $post->setUpdatedAt($timeNow);
    }
}