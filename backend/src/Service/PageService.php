<?php

namespace App\Service;

use App\Entity\Page;
use App\Service\Menu\ItemServiceInterface;

readonly class PageService implements PageServiceInterface
{
    public function __construct(
        protected ItemServiceInterface $menuItemService,
    )
    {

    }

    /** {@inheritdoc } */
    public function postUpdatePageSlugChange(Page $page, string $oldSlug): void
    {
        $this->menuItemService->updateInternalLinkRouteByPage($page, $oldSlug);
    }

    /** {@inheritdoc } */
    public function cleanupAfterDeletePost(Page $page): void
    {
        $this->menuItemService->deleteAllByInternalLinkPath($page->getFrontendRoute());
    }
}