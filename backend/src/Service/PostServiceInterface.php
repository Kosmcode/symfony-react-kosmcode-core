<?php

namespace App\Service;

use App\Entity\Post;

interface PostServiceInterface
{
    /**
     * @param Post $post
     * @return void
     */
    public function preparePost(Post $post): void;

    /**
     * @param Post $post
     *
     * @return void
     */
    public function cleanupAfterDeletePost(Post $post): void;

    /**
     * @param Post $post
     * @param string $oldSlug
     *
     * @return void
     */
    public function postUpdatePageSlugChange(Post $post, string $oldSlug): void;
}