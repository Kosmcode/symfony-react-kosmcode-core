<?php

namespace App\Service;

use App\Entity\Page;

interface PageServiceInterface
{
    /**
     * @param Page $page
     * @param string $oldSlug
     *
     * @return void
     */
    public function postUpdatePageSlugChange(Page $page, string $oldSlug): void;

    /**
     * @param Page $page
     *
     * @return void
     */
    public function cleanupAfterDeletePost(Page $page): void;
}