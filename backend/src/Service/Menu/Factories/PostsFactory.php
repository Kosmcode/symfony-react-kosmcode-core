<?php

namespace App\Service\Menu\Factories;

use App\Entity\Post;
use App\Enum\RouteEnum;
use App\Repository\PostRepository;

readonly class PostsFactory implements FactoryInterface
{
    public function __construct(
        private PostRepository $postRepository,
    )
    {
    }

    public function getToEasyAdminOptions(): array
    {
        $output = [
            'Posts' => RouteEnum::posts->value
        ];

        $publishedPosts = $this->postRepository->getAllPublishedPosts();

        /** @var Post $post */
        foreach ($publishedPosts as $post) {
            $output['Post: ' . $post->getTitle()] = $post->getFrontendRoute();
        }

        return $output;
    }
}