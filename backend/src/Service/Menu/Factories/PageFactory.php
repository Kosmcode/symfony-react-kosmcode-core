<?php

namespace App\Service\Menu\Factories;

use App\Entity\Page;
use App\Repository\PageRepository;

class PageFactory implements FactoryInterface
{
    public function __construct(
        protected PageRepository $pageRepository,
    )
    {

    }

    /** {@inheritdoc } */
    public function getToEasyAdminOptions(): array
    {
        $allPublishedPages = $this->pageRepository->getAllPublished();

        $options = [];

        /** @var Page $page */
        foreach ($allPublishedPages as $page) {
            $options['Page: ' . $page->getTitle()] = $page->getFrontendRoute();
        }

        return $options;
    }
}