<?php

namespace App\Service\Menu\Factories;

use App\Enum\RouteEnum;

class HomeFactory implements FactoryInterface
{
    /** {@inheritdoc } */
    public function getToEasyAdminOptions(): array
    {
        return [
            'Home' => RouteEnum::home->value,
        ];
    }
}