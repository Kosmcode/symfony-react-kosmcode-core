<?php

namespace App\Service\Menu\Factories;

interface FactoryInterface
{
    /**
     * @return array<string, string>
     */
    public function getToEasyAdminOptions(): array;
}