<?php

namespace App\Service\Menu;

interface FactoryInterface
{
    /**
     * @return array<string, string>
     */
    public function getToEasyAdminOptions(): array;
}