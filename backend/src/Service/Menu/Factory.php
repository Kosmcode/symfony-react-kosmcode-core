<?php

namespace App\Service\Menu;

use App\Service\Menu\Factories\FactoryInterface as MenuItemFactoryInterface;

class Factory implements FactoryInterface
{
    /**
     * @param MenuItemFactoryInterface[] $factories
     */
    public function __construct(
        private readonly iterable $factories,
    )
    {

    }

    /** {@inheritdoc} */
    public function getToEasyAdminOptions(): array
    {
        $easyAdminOptions = [];

        foreach ($this->factories as $factory) {
            $easyAdminOptions = array_merge($easyAdminOptions, $factory->getToEasyAdminOptions());
        }

        return $easyAdminOptions;
    }
}