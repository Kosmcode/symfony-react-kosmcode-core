<?php

namespace App\Service\Menu;

use App\Entity\Menu\Item;
use App\Entity\Page;
use App\Entity\Post;

interface ItemServiceInterface
{
    /**
     * @param Page $page
     * @param string $oldSlug
     * @return void
     */
    public function updateInternalLinkRouteByPage(Page $page, string $oldSlug): void;

    /**
     * @param string $internalLinkPath
     * @return void
     */
    public function deleteAllByInternalLinkPath(string $internalLinkPath): void;

    /**
     * @param Item $item
     * @param int $oldPosition
     *
     * @return void
     */
    public function changeItemPositionByOld(Item $item, int $oldPosition): void;

    /**
     * @param Item $item
     *
     * @return void
     */
    public function setLastPositionItem(Item $item): void;

    /**
     * @param Post $post
     * @param string $oldSlug
     *
     * @return void
     */
    public function updateInternalLinkRouteByPost(Post $post, string $oldSlug): void;
}