<?php

namespace App\Service\Menu;

use App\Entity\Menu\Item;
use App\Entity\Page;
use App\Entity\Post;
use App\Repository\Menu\ItemRepositoryInterface;

readonly class ItemService implements ItemServiceInterface
{
    public function __construct(
        protected ItemRepositoryInterface $menuItemRepository,
    )
    {

    }

    /** {@inheritdoc } */
    public function updateInternalLinkRouteByPage(Page $page, string $oldSlug): void
    {
        $menuItemsWithSlug = $this->menuItemRepository->findBy([
            Item::FIELD_INTERNAL_LINK_PATH => $page->prepareFrontendRoute($oldSlug),
        ]);

        if (!$menuItemsWithSlug) {
            return;
        }

        /** @var Item $menuItemWithSlug */
        foreach ($menuItemsWithSlug as $menuItemWithSlug) {
            $menuItemWithSlug->setInternalLinkPath(
                $page->getFrontendRoute()
            );

            $this->menuItemRepository->persist($menuItemWithSlug);
        }

        $this->menuItemRepository->flush();
    }

    /** {@inheritdoc } */
    public function deleteAllByInternalLinkPath(string $internalLinkPath): void
    {
        $menuItemsWithSlug = $this->menuItemRepository->findBy([
            Item::FIELD_INTERNAL_LINK_PATH => $internalLinkPath,
        ]);

        if (!$menuItemsWithSlug) {
            return;
        }

        /** @var Item $menuItemWithSlug */
        foreach ($menuItemsWithSlug as $menuItemWithSlug) {
            $this->menuItemRepository->remove($menuItemWithSlug);
        }

        $this->menuItemRepository->flush();

        $this->reorganizePositionCorrectOrder();
    }

    /** {@inheritdoc } */
    public function changeItemPositionByOld(Item $item, int $oldPosition): void
    {
        $itemToChangePosition = $this->menuItemRepository->findOneByPositionAndNotEqualId(
            $item->getPosition(), // @phpstan-ignore-line
            $item->getId() // @phpstan-ignore-line
        );

        if (!$itemToChangePosition) {
            return;
        }

        $itemToChangePosition->setPosition($oldPosition);

        $this->menuItemRepository->persist($itemToChangePosition, true); // @phpstan-ignore-line
    }

    /**
     * @param Item $item
     *
     * @return void
     */
    public function setLastPositionItem(Item $item): void
    {
        $itemsCount = $this->menuItemRepository->getLastItemPosition();

        $item->setPosition($itemsCount + 1);
    }

    /** {@inheritdoc } */
    public function updateInternalLinkRouteByPost(Post $post, string $oldSlug): void
    {
        $menuItemsWithSlug = $this->menuItemRepository->findBy([
            Item::FIELD_INTERNAL_LINK_PATH => $post->prepareFrontendRoute($oldSlug),
        ]);

        if (!$menuItemsWithSlug) {
            return;
        }

        /** @var Item $menuItemWithSlug */
        foreach ($menuItemsWithSlug as $menuItemWithSlug) {
            $menuItemWithSlug->setInternalLinkPath(
                $post->getFrontendRoute()
            );

            $this->menuItemRepository->persist($menuItemWithSlug);
        }

        $this->menuItemRepository->flush();
    }

    /**
     * @return void
     */
    protected function reorganizePositionCorrectOrder(): void
    {
        $allMenuItems = $this->menuItemRepository->findBy(
            [],
            [
                Item::FIELD_POSITION => ItemRepositoryInterface::ORDER_ASC,
            ],
        );

        if (!$allMenuItems) {
            return;
        }

        $positionCounter = 1;

        /** @var Item $menuItem */
        foreach ($allMenuItems as $menuItem) {
            $menuItem->setPosition($positionCounter);

            $this->menuItemRepository->persist($menuItem);

            $positionCounter++;
        }

        $this->menuItemRepository->flush();
    }
}