<?php

namespace App\Service;

use Symfony\Component\Security\Core\User\UserInterface;

interface UserServiceInterface
{
    /**
     * @return bool
     */
    public function userIsLogged(): bool;

    /**
     * @return UserInterface|null
     */
    public function getUser(): ?UserInterface;
}
