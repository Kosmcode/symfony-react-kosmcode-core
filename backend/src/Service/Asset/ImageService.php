<?php

namespace App\Service\Asset;

use App\Entity\Asset\Image;
use App\Enum\Asset\PathEnum;
use RuntimeException;
use Symfony\Component\HttpKernel\KernelInterface;

readonly class ImageService implements ImageServiceInterface
{
    public function __construct(
        protected KernelInterface $kernel,
    ) {
    }

    /** @inheritDoc */
    public function convertImageToWebpAndPrepareBase64ImageByImage(Image $image): void
    {
        $imageFilepath = $this->kernel->getProjectDir().PathEnum::uploadsImages->value
            .$image->getFilename();

        $fileInfo = pathinfo($imageFilepath);

        $newFilename = $fileInfo['filename'].'.webp';
        $newFilepath = $this->kernel->getProjectDir().PathEnum::uploadsImages->value
            .$fileInfo['filename'].'.webp';

        $imageData = file_get_contents(
            $this->kernel->getProjectDir().PathEnum::uploadsImages->value
            .$image->getFilename()
        );

        if (!$imageData) {
            throw new RuntimeException('Unable to open image');
        }

        $imageHandler = imagecreatefromstring($imageData);

        if (!$imageHandler) {
            throw new RuntimeException('Unable to create image');
        }

        imagewebp($imageHandler, $newFilepath);

        $image->setFilename($newFilename);

        unlink($imageFilepath);

        $imageData = file_get_contents($newFilepath);

        if (!$imageData) {
            throw new RuntimeException('Unable to open webp image');
        }

        $base64Image = base64_encode($imageData);

        imagedestroy($imageHandler);

        $image->setBase64($base64Image);
    }

    /** @inheritDoc */
    public function unlinkImageByImage(Image $image): void
    {
        $imageFilepath = $this->kernel->getProjectDir().PathEnum::uploadsImages->value
            .$image->getFilename();

        unlink($imageFilepath);
    }
}
