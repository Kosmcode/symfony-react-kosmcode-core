<?php

namespace App\Service\Asset;

use App\Entity\Asset\Image;

interface ImageServiceInterface
{
    /**
     * @param Image $image
     *
     * @return void
     */
    public function convertImageToWebpAndPrepareBase64ImageByImage(Image $image): void;

    /**
     * @param Image $image
     *
     * @return void
     */
    public function unlinkImageByImage(Image $image): void;
}
