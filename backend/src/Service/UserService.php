<?php

namespace App\Service;

use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\Security\Core\User\UserInterface;

readonly class UserService implements UserServiceInterface
{
    public function __construct(
        protected Security $security,
    )
    {
    }

    /** {@inheritDoc} */
    public function userIsLogged(): bool
    {
        return (bool)$this->security->getUser();
    }

    /** {@inheritDoc} */
    public function getUser(): ?UserInterface
    {
        return $this->security->getUser();
    }
}
