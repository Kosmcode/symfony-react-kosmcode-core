<?php

namespace App\Exceptions;

use Exception;

class EnumException extends Exception
{
    protected $code = 500; // @phpstan-ignore-line
}
