# [WIP!] Symfony React KosmCODE Core

## Tech stack
* Symfony 7
* PHP 8.2
* MySQL 8
* Docker
* React
* Api Platform + GraphQL
* Easy Admin
* Used free template `Clean Blog` from https://startbootstrap.com/theme/clean-blog (https://github.com/startbootstrap/startbootstrap-clean-blog)

## Quick setup
1. Build docker
```shell
make build
```
2. Make setup new
```shell
make setup-new
```
3. Enjoy!

## Urls (default)
* `http://localhost` - frontend React App
* `http://localhost:81` - backend admin
* `http://localhost:81/login` - login page to Admin

## Useful
* `make local-services-up` - up local docker services (PhpMyAdmin etc)
* all commands in MakeFile