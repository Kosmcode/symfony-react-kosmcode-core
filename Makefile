.PHONY: up
up:
	docker compose --file=./docker-compose.yml up -d

.PHONY: down
down:
	docker compose --file=./docker-compose.yml down

.PHONY: build
build:
	docker compose --file=./docker-compose.yml up -d --build

.PHONY: start
start:
	docker compose --file=./docker-compose.yml start

.PHONY: stop
stop:
	docker compose --file=./docker-compose.yml stop

.PHONY: restart
restart:
	make stop
	make start

.PHONY: local-services-up
local-services-up:
	docker compose --file=./docker-compose.local-services.yml up -d

.PHONY: local-services-down
local-services-down:
	docker compose --file=./docker-compose.local-services.yml down

.PHONY: local-services-build
local-services-build:
	docker compose --file=./docker-compose.local-services.yml up -d --build

.PHONY: backend-bash
backend-bash:
	docker compose --file=./docker-compose.yml exec --user=1000 backend bash

.PHONY: grumphp-run
grumphp-run:
	cd ./backend && ./vendor/bin/grumphp run -n

.PHONY: grumphp-git-hooks-init
grumphp-git-hooks-init:
	cp ./dev/git/pre-commit.sample ./.git/hooks/pre-commit

.PHONY: frontend-bash
frontend-bash:
	docker compose --file=./docker-compose.yml exec frontend bash

.PHONY: user-admin-create
user-admin-create:
	@echo "Enter Username:"; \
    read USERNAME; \
    echo "Enter E-mail:"; \
    read EMAIL; \
    echo "Enter Password:"; \
    read PASSWORD; \
    docker compose --file=./docker-compose.yml exec backend bin/console nucleos:user:create --super-admin $$USERNAME $$EMAIL $$PASSWORD; \
    docker compose --file=./docker-compose.yml exec backend bin/console nucleos:user:promote $$USERNAME ROLE_ADMIN

.PHONY: setup-new
setup-new:
	make frontend-install-all
	make backend-install-all
	docker compose --file=./docker-compose.yml exec backend bin/console d:s:d --force
	docker compose --file=./docker-compose.yml exec backend bin/console d:s:c
	docker compose --file=./docker-compose.yml exec backend bin/console d:s:u --force
	make user-admin-create
	make sample-data-append


.PHONY: all-up
all-up:
	make up
	make local-services-up

.PHONY: sample-data-append
sample-data-append:
	@echo "Append sample fixtures data ? (y/n)"; \
    read SAMPLEDATA; \
	if [ $$SAMPLEDATA = "y" ]; then \
		docker compose --file=./docker-compose.yml exec backend bin/console d:f:l --append; \
    fi

.PHONY: backend-install-all
backend-install-all:
	docker compose --file=./docker-compose.yml exec backend composer install
	docker compose --file=./docker-compose.yml exec backend npm install
	docker compose --file=./docker-compose.yml exec backend npm run build

.PHONY: frontend-install-all
frontend-install-all:
	docker compose --file=./docker-compose.yml exec frontend npm install
	docker compose --file=./docker-compose.yml exec frontend npm run build