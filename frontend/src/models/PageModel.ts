import HeaderImageModel from "./HeaderImageModel";

export default class PageModel {
    content = String;
    metaTitle = String;
    metaKeywords = String;
    metaDescription = String;
    headerDescription = String;
    headerImage = HeaderImageModel;

    constructor(model: Partial<PageModel> = {}) {
        Object.assign(this, model);
    }
}
