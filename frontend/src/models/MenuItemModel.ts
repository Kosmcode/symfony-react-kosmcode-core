export default class MenuItemModel {
    title = String;
    type = Number;
    position = Number;
    internalLinkPath = String;
    externalLinkUrl = String;

    constructor(model: Partial<MenuItemModel> = {}) {
        Object.assign(this, model);
    }
}