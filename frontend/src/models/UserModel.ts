export default class UserModel {
    username: string | undefined;

    constructor(model: Partial<UserModel> = {}) {
        Object.assign(this, model);
    }
}