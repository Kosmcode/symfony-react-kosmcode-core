import UserModel from "./UserModel";
import HeaderImageModel from "./HeaderImageModel";

export default class PostModel {
    static content: string;
    static updatedAt: string;
    static metaTitle: string;
    static metaKeywords: string;
    static metaDescription: string;
    static headerImage: HeaderImageModel;
    static slug: string;
    static title: string;
    static headerDescription: string;
    static user: UserModel;
    static createdAt: string;

    constructor(model: Partial<PostModel> = {}) {
        Object.assign(this, model);
    }
}