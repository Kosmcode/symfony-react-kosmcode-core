export default class PaginationInfoModel {
    hasNextPage = Boolean;

    constructor(model: Partial<PaginationInfoModel> = {}) {
        Object.assign(this, model);
    }
}