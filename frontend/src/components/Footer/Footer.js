import React from 'react';
import PropTypes from 'prop-types';
import styles from './Footer.scss';

const Footer = props => (
	<footer className="border-top">
		<div className="container px-4 px-lg-5">
			<div className="row gx-4 gx-lg-5 justify-content-center">
				<div className="col-md-10 col-lg-8 col-xl-7">
					<ul className="list-inline text-center">
						<li className="list-inline-item">
							<a href="#!">
                                    <span className="fa-stack fa-lg">
                                        <i className="fas fa-circle fa-stack-2x"></i>
                                        <i className="fab fa-twitter fa-stack-1x fa-inverse"></i>
                                    </span>
							</a>
						</li>
						<li className="list-inline-item">
							<a href="#!">
                                    <span className="fa-stack fa-lg">
                                        <i className="fas fa-circle fa-stack-2x"></i>
                                        <i className="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                                    </span>
							</a>
						</li>
						<li className="list-inline-item">
							<a href="#!">
                                    <span className="fa-stack fa-lg">
                                        <i className="fas fa-circle fa-stack-2x"></i>
                                        <i className="fab fa-github fa-stack-1x fa-inverse"></i>
                                    </span>
							</a>
						</li>
					</ul>
					<div className="small text-center text-muted fst-italic">Copyright &copy; Your Website 2023</div>
				</div>
			</div>
		</div>
	</footer>
);

// todo: Unless you need to use lifecycle methods or local state,
// write your component in functional form as above and delete
// this section. 
// class Footer extends React.Component {
//   render() {
//     return <div>This is a component called Footer.</div>;
//   }
// }

const FooterPropTypes = {
	// always use prop types!
};

Footer.propTypes = FooterPropTypes;

export default Footer;
