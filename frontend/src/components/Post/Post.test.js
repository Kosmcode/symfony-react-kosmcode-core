import React from 'react';
import { shallow, render, mount } from 'enzyme';
import Post from './Post';

describe('Post', () => {
  let props;
  let shallowPost;
  let renderedPost;
  let mountedPost;

  const shallowTestComponent = () => {
    if (!shallowPost) {
      shallowPost = shallow(<Post {...props} />);
    }
    return shallowPost;
  };

  const renderTestComponent = () => {
    if (!renderedPost) {
      renderedPost = render(<Post {...props} />);
    }
    return renderedPost;
  };

  const mountTestComponent = () => {
    if (!mountedPost) {
      mountedPost = mount(<Post {...props} />);
    }
    return mountedPost;
  };  

  beforeEach(() => {
    props = {};
    shallowPost = undefined;
    renderedPost = undefined;
    mountedPost = undefined;
  });

  // Shallow / unit tests begin here
 
  // Render / mount / integration tests begin here
  
});
