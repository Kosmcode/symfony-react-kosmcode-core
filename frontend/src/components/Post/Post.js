import React from 'react';
//import PropTypes from 'prop-types';
//import styles from './Post.scss';
import {useParams} from "react-router-dom";
import PostRepository from "../../repositories/graphql/PostRepository";
import ErrorPage from "../ErrorPage";
import PostModel from "../../models/PostModel";
import MetaService from "../../services/MetaService";
import Header from "../Header";
import DocumentMeta from 'react-document-meta';
import {useQuery} from "react-query";

const Post = props => {

    const {postSlug} = useParams();

    let metaService = new MetaService();
    metaService.appendToCanonicalLinkPage('post/' + postSlug);

    let postRepository = new PostRepository();

    const {data, isLoading, error} = useQuery('slugQueryPost', () => {
        return postRepository.getPostBySlug(postSlug)
    });

    if (isLoading) return "Loading...";
    if (error) {
        return <ErrorPage errorCode={500} errorMessage={'Something went wrong, please try again.'}/>;
    }

    if (typeof data.slugQueryPost === 'undefined') return <ErrorPage/>;

    let postModel = new PostModel(data.slugQueryPost);

    metaService.metaTitle = postModel.metaTitle;
    metaService.metaDescription = postModel.metaDescription;
    metaService.metaKeywords = postModel.metaKeywords;

    return (
        <DocumentMeta {...metaService.getMetaObjectForDocumentMeta()}>
            <Header
                title={postModel.title}
                backgroundImageBase64={postModel.headerImage.base64 ?? null}
                description={postModel.headerDescription}
            />

            <article className="mb-4">
                <div className="container px-4 px-lg-5">
                    <div className="row gx-4 gx-lg-5 justify-content-center">
                        <div
                            className="col-md-10 col-lg-8 col-xl-7"
                            dangerouslySetInnerHTML={{__html: postModel.content}}
                        >
                        </div>
                    </div>
                </div>
            </article>
        </DocumentMeta>
    );
};

// todo: Unless you need to use lifecycle methods or local state,
// write your component in functional form as above and delete
// this section. 
// class Post extends React.Component {
//   render() {
//     return <div>This is a component called Post.</div>;
//   }
// }

const PostPropTypes = {
    // always use prop types!
};

Post.propTypes = PostPropTypes;

export default Post;
