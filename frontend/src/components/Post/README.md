# Post

<!-- STORY -->

## Introduction

Post is an easy-to-use component.

## Usage

```javascript
import { Post } from 'some-package-name';
```

## Example use

```javascript
const myPage = props => {
  return (
    <main>
      <Post />
    </main>
  );
};
```

## Properties

- `className` - is the class name of the component

| propName  | propType | defaultValue | isRequired |
| --------- | -------- | ------------ | ---------- |
| className | string   | -            | -          |
