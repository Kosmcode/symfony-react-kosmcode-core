import React from 'react';
import { shallow, render, mount } from 'enzyme';
import Page from './Page';

describe('Page', () => {
  let props;
  let shallowPage;
  let renderedPage;
  let mountedPage;

  const shallowTestComponent = () => {
    if (!shallowPage) {
      shallowPage = shallow(<Page {...props} />);
    }
    return shallowPage;
  };

  const renderTestComponent = () => {
    if (!renderedPage) {
      renderedPage = render(<Page {...props} />);
    }
    return renderedPage;
  };

  const mountTestComponent = () => {
    if (!mountedPage) {
      mountedPage = mount(<Page {...props} />);
    }
    return mountedPage;
  };  

  beforeEach(() => {
    props = {};
    shallowPage = undefined;
    renderedPage = undefined;
    mountedPage = undefined;
  });

  // Shallow / unit tests begin here
 
  // Render / mount / integration tests begin here
  
});
