# Page

<!-- STORY -->

## Introduction

Page is an easy-to-use component.

## Usage

```javascript
import { Page } from 'some-package-name';
```

## Example use

```javascript
const myPage = props => {
    return (
        <main>
            <PageModel/>
        </main>
    );
};
```

## Properties

- `className` - is the class name of the component

| propName  | propType | defaultValue | isRequired |
| --------- | -------- | ------------ | ---------- |
| className | string   | -            | -          |
