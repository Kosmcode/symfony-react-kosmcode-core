import React from 'react';
import {useParams} from 'react-router-dom';
import DocumentMeta from 'react-document-meta';
import PageModel from '../../models/PageModel';
import PageRepository from "../../repositories/graphql/PageRepository";
import MetaService from "../../services/MetaService";
import ErrorPage from "../ErrorPage";
import Header from "../Header";
import {useQuery} from "react-query";

const Page = () => {

    const {pageSlug} = useParams();

    let metaService = new MetaService();
    metaService.appendToCanonicalLinkPage(pageSlug);

    let pageRepository = new PageRepository();

    const {data, isLoading, error} = useQuery('page', () => {
        return pageRepository.getPageBySlug(pageSlug);
    });

    if (isLoading) return "Loading...";
    if (error) {
        return <ErrorPage errorCode={500} errorMessage={'Something went wrong, please try again.'}/>;
    }

    if (!data.slugQueryPage) return <ErrorPage/>;

    let Page = new PageModel(data.slugQueryPage);

    metaService.metaTitle = Page.metaTitle;
    metaService.metaDescription = Page.metaDescription;
    metaService.metaKeywords = Page.metaKeywords;

    return (
        <DocumentMeta {...metaService.getMetaObjectForDocumentMeta()}>
            <Header
                title={Page.metaTitle}
                backgroundImageBase64={Page.headerImage.base64 ?? null}
                description={Page.headerDescription}
            />

            <main className="mb-4">
                <div className="container px-4 px-lg-5">
                    <div className="row gx-4 gx-lg-5 justify-content-center">
                        <div
                            className="col-md-10 col-lg-8 col-xl-7"
                            dangerouslySetInnerHTML={{__html: Page.content}}>
                        </div>
                    </div>
                </div>
            </main>
        </DocumentMeta>
    )
        ;
}
export default Page;
