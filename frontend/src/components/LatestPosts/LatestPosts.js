import React from 'react';
import PostRepository from "../../repositories/graphql/PostRepository";
import ErrorPage from "../ErrorPage";
import PostModel from "../../models/PostModel";
import {Link} from "react-router-dom";
import {useQuery} from "react-query";
import PostService from "../../services/template/PostService";
//import PropTypes from 'prop-types';
//import styles from './LatestPosts.scss';

const LatestPosts = props => {

    let postRepository = new PostRepository();

    const {data, isLoading, error} = useQuery('paginatedPost', () => {
        return postRepository.getPaginatedPost(1);
    });

    if (isLoading) return "Loading...";
    if (error) {
        return <ErrorPage errorCode={500} errorMessage={'Something went wrong, please try again.'}/>;
    }

    if (typeof data.posts.collection === 'undefined') return <ErrorPage/>;

    let postsList = [];

    let templatePostService = new PostService();

    data.posts.collection.forEach((item, index) => {
        let postModel = new PostModel(item);

        postsList.push(
            templatePostService.renderPost(postModel, index)
        );
    });

    return (
        <div className="container px-4 px-lg-5">
            <div className="row gx-4 gx-lg-5 justify-content-center">
                <div className="col-md-10 col-lg-8 col-xl-7">

                    {postsList}

                    <div className="d-flex justify-content-end mb-4">
                        <Link
                            to="posts"
                            className="btn btn-primary text-uppercase"
                        >
                            All Posts →
                        </Link>
                    </div>
                </div>
            </div>
        </div>
    );
};

const LatestPostsPropTypes = {
    // always use prop types!
};

LatestPosts.propTypes = LatestPostsPropTypes;

export default LatestPosts;
