import React from 'react';
import { shallow, render, mount } from 'enzyme';
import LatestPosts from './LatestPosts';

describe('LatestPosts', () => {
  let props;
  let shallowLatestPosts;
  let renderedLatestPosts;
  let mountedLatestPosts;

  const shallowTestComponent = () => {
    if (!shallowLatestPosts) {
      shallowLatestPosts = shallow(<LatestPosts {...props} />);
    }
    return shallowLatestPosts;
  };

  const renderTestComponent = () => {
    if (!renderedLatestPosts) {
      renderedLatestPosts = render(<LatestPosts {...props} />);
    }
    return renderedLatestPosts;
  };

  const mountTestComponent = () => {
    if (!mountedLatestPosts) {
      mountedLatestPosts = mount(<LatestPosts {...props} />);
    }
    return mountedLatestPosts;
  };  

  beforeEach(() => {
    props = {};
    shallowLatestPosts = undefined;
    renderedLatestPosts = undefined;
    mountedLatestPosts = undefined;
  });

  // Shallow / unit tests begin here
 
  // Render / mount / integration tests begin here
  
});
