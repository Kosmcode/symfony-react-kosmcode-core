# LatestPosts

<!-- STORY -->

## Introduction

LatestPosts is an easy-to-use component.

## Usage

```javascript
import { LatestPosts } from 'some-package-name';
```

## Example use

```javascript
const myPage = props => {
  return (
    <main>
      <LatestPosts />
    </main>
  );
};
```

## Properties

- `className` - is the class name of the component

| propName  | propType | defaultValue | isRequired |
| --------- | -------- | ------------ | ---------- |
| className | string   | -            | -          |
