import React from 'react';
import { shallow, render, mount } from 'enzyme';
import Posts from './Posts';

describe('Posts', () => {
  let props;
  let shallowPosts;
  let renderedPosts;
  let mountedPosts;

  const shallowTestComponent = () => {
    if (!shallowPosts) {
      shallowPosts = shallow(<Posts {...props} />);
    }
    return shallowPosts;
  };

  const renderTestComponent = () => {
    if (!renderedPosts) {
      renderedPosts = render(<Posts {...props} />);
    }
    return renderedPosts;
  };

  const mountTestComponent = () => {
    if (!mountedPosts) {
      mountedPosts = mount(<Posts {...props} />);
    }
    return mountedPosts;
  };  

  beforeEach(() => {
    props = {};
    shallowPosts = undefined;
    renderedPosts = undefined;
    mountedPosts = undefined;
  });

  // Shallow / unit tests begin here
 
  // Render / mount / integration tests begin here
  
});
