import React from 'react';
//import PropTypes from 'prop-types';
//import styles from './Posts.scss';
import {Link, useParams} from "react-router-dom";
import PostRepository from "../../repositories/graphql/PostRepository";
import ErrorPage from "../ErrorPage";
import PostModel from "../../models/PostModel";
import Header from "../Header";
import DocumentMeta from 'react-document-meta';
import MetaService from "../../services/MetaService";
import PaginationInfoModel from "../../models/PaginationInfoModel";
import {useQuery} from "react-query";
import FormatService from "../../services/datatime/FormatService";
import PostService from "../../services/template/PostService";

const Posts = props => {

	const {page} = useParams();

	let currentPage = 1;
	let metaService = new MetaService();

	preparePageParam();

	function preparePageParam() {
		if (typeof page !== "undefined") {
			currentPage = parseInt(page);
			metaService.appendToCanonicalLinkPage('posts/' + page);

			return;
		}

		metaService.appendToCanonicalLinkPage('posts');
	}

	let postRepository = new PostRepository();

	const {data, isLoading, error} = useQuery('paginatedPost', () => {
		return postRepository.getPaginatedPost(currentPage);
	});

	if (isLoading) return "Loading...";
	if (error) {
		return <ErrorPage errorCode={500} errorMessage={'Something went wrong, please try again.'}/>;
	}

	if (typeof data.posts.collection === 'undefined') return <ErrorPage/>;

	if (typeof data.posts.paginationInfo === 'undefined') return <ErrorPage/>;

	let pageInfoModel = new PaginationInfoModel(data.posts.paginationInfo);

	let postsList = [];

	let templatePostService = new PostService();

	data.posts.collection.forEach((item, index) => {
		let postModel = new PostModel(item);

		postsList.push(
			templatePostService.renderPost(postModel, index)
		);
	});

	metaService.metaTitle = 'Posts';
	metaService.metaDescription = 'Page with posts';
	metaService.metaKeywords = 'page, posts, post';

	function renderPostNavButtons(pageInfoModel = PaginationInfoModel) {
		let renderPostNavs = [];

		renderPostNavs.push();

		if (currentPage >= 2) {
			let route = '/posts/' + (currentPage - 1);
			renderPostNavs.push(<Link className="btn btn-primary text-uppercase" to={route} reloadDocument>Newest Posts</Link>)
		}

		if (pageInfoModel.hasNextPage) {
			let route = '/posts/' + (currentPage +1);
			renderPostNavs.push(<Link className="btn btn-primary text-uppercase" to={route} reloadDocument>Oldest Posts</Link>)
		}

		return renderPostNavs;
	}

	let navsButtons = renderPostNavButtons(pageInfoModel);

	return (
		<DocumentMeta {...metaService.getMetaObjectForDocumentMeta()}>
			<Header
				title="Posts"
			/>

			<div className="container px-4 px-lg-5">
				<div className="row gx-4 gx-lg-5 justify-content-center">
					<div className="col-md-10 col-lg-8 col-xl-7">

						{postsList}

						<div className="d-flex mb-4 justify-content-between">
							{navsButtons}
						</div>

					</div>
				</div>
			</div>

		</DocumentMeta>
	);
};

// todo: Unless you need to use lifecycle methods or local state,
// write your component in functional form as above and delete
// this section. 
// class Posts extends React.Component {
//   render() {
//     return <div>This is a component called Posts.</div>;
//   }
// }

const PostsPropTypes = {
	// always use prop types!
};

Posts.propTypes = PostsPropTypes;

export default Posts;
