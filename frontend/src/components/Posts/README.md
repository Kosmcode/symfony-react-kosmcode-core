# Posts

<!-- STORY -->

## Introduction

Posts is an easy-to-use component.

## Usage

```javascript
import { Posts } from 'some-package-name';
```

## Example use

```javascript
const myPage = props => {
  return (
    <main>
      <Posts />
    </main>
  );
};
```

## Properties

- `className` - is the class name of the component

| propName  | propType | defaultValue | isRequired |
| --------- | -------- | ------------ | ---------- |
| className | string   | -            | -          |
