import React from 'react';
import {Link} from "react-router-dom";

export default class ErorrPage extends React.Component {

    _errorCode = 404;
    _errorMessage = 'Page not found';

    constructor(props) {
        super(props);
    }

    render() {

        if (typeof this.props.errorCode != 'undefined') {
            this._errorCode = this.props.errorCode;
        }

        if (typeof this.props.errorMessage != 'undefined') {
            this._errorMessage = this.props.errorMessage;
        }

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="error-template">
                            <h1>
                                Oops!</h1>
                            <h2>
                                Error {this._errorCode}
                            </h2>
                            <div className="error-details">
                                {this._errorMessage}
                            </div>
                            <div className="error-actions">
                                <Link className="btn btn-primary btn-lg" to="/">
                                    <span className="glyphicon glyphicon-home"></span>
                                    Home
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
