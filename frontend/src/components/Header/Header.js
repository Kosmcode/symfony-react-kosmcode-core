import React from 'react';
// import PropTypes from 'prop-types';
// import styles from './Header.scss';

export default class Header extends React.Component {

	_backgroundImageUrl = "url('/assets/img/home-bg.jpg')";

	constructor(props) {
		super(props);
	}

	prepareBackgroundImageUrl() {
		if (typeof this.props.backgroundImageUrl != 'undefined') {
			this._backgroundImageUrl = "url('" + this.props.backgroundImageUrl + "')";

			return;
		}

		if (typeof this.props.backgroundImageBase64 != 'undefined') {
			this._backgroundImageUrl = "url('data:image/png;base64," + this.props.backgroundImageBase64 + "')";
		}
	}

	render() {
		this.prepareBackgroundImageUrl();

		return (
			<header className="masthead" style={{ backgroundImage: this._backgroundImageUrl }}>
				<div className="container position-relative px-4 px-lg-5">
					<div className="row gx-4 gx-lg-5 justify-content-center">
						<div className="col-md-10 col-lg-8 col-xl-7">
							<div className="site-heading">
								<h1>{ this.props.title }</h1>
								{typeof this.props.description != 'undefined' &&
									<span className="subheading">
										{ this.props.description }
									</span>
								}
							</div>
						</div>
					</div>
				</div>
			</header>
		);
	}
}

