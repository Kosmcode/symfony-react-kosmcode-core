import React from 'react';
import { Outlet } from "react-router-dom";
import Nav from "../Nav";
import Footer from "../Footer";
// import styles from './Layout.scss';

class Layout extends React.Component {
  render() {
      return <>
          <Nav />

          <Outlet />

          <Footer />
      </>;
    }
}

export default Layout;