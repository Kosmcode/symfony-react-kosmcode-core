import React from 'react';
import { shallow, render, mount } from 'enzyme';
import Layout from './Layout';

describe('Layout', () => {
  let props;
  let shallowLayout;
  let renderedLayout;
  let mountedLayout;

  const shallowTestComponent = () => {
    if (!shallowLayout) {
      shallowLayout = shallow(<Layout {...props} />);
    }
    return shallowLayout;
  };

  const renderTestComponent = () => {
    if (!renderedLayout) {
      renderedLayout = render(<Layout {...props} />);
    }
    return renderedLayout;
  };

  const mountTestComponent = () => {
    if (!mountedLayout) {
      mountedLayout = mount(<Layout {...props} />);
    }
    return mountedLayout;
  };  

  beforeEach(() => {
    props = {};
    shallowLayout = undefined;
    renderedLayout = undefined;
    mountedLayout = undefined;
  });

  // Shallow / unit tests begin here
 
  // Render / mount / integration tests begin here
  
});
