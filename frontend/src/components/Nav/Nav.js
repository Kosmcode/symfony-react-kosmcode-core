import React from 'react';
// import PropTypes from 'prop-types';
// import styles from './Nav.scss';
import {Link} from "react-router-dom";
import ErrorPage from "../ErrorPage";
import MenuItemRepository from "../../repositories/graphql/MenuItemRepository";
import { useEffect } from 'react';
import MenuItemModel from "../../models/MenuItemModel";
import {useQuery} from "react-query";

const Nav = () => {

    const INTERNAL_TYPE_LINK = 1;

    useEffect(() => {
        let scrollPos = 0;
        const mainNav = document.getElementById('root');
        const headerHeight = mainNav.clientHeight;
        window.addEventListener('scroll', function () {
            const currentTop = document.body.getBoundingClientRect().top * -1;
            if (currentTop < scrollPos) {
                // Scrolling Up
                if (currentTop > 0 && mainNav.classList.contains('is-fixed')) {
                    mainNav.classList.add('is-visible');
                } else {
                    mainNav.classList.remove('is-visible', 'is-fixed');
                }
            } else {
                // Scrolling Down
                mainNav.classList.remove(['is-visible']);
                if (currentTop > headerHeight && !mainNav.classList.contains('is-fixed')) {
                    mainNav.classList.add('is-fixed');
                }
            }
            scrollPos = currentTop;
        });
    }, []);

    let menuItemRepository = new MenuItemRepository();

    const {data, isLoading, error} = useQuery('items', () => {
        return menuItemRepository.getMenuItems();
    });

    if (isLoading) return "Loading...";
    if (error) {
        return <ErrorPage errorCode={500} errorMessage={'Something went wrong, please try again.'}/>;
    }

    if (typeof data.items.edges === 'undefined') return <ErrorPage/>;

    let menuItems = [];

    data.items.edges.forEach((item, index) => {
        let menuItemModel = new MenuItemModel(item.node);

        if (menuItemModel.type === INTERNAL_TYPE_LINK) {
            menuItems.push(
                <li key={index} className="nav-item">
                    <Link
                        className="nav-link px-lg-3 py-3 py-lg-4"
                        to={menuItemModel.internalLinkPath}
                        reloadDocument
                    >
                        {menuItemModel.title}
                    </Link>
                </li>
            );

            return;
        }

        menuItems.push(
            <li key={index} className="nav-item">
                <a className="nav-link px-lg-3 py-3 py-lg-4" href={menuItemModel.externalLinkUrl} target='_blank'>
                    {menuItemModel.title}
                </a>
            </li>
        );
    })

    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-secondary bg-opacity-75" id="mainNav">
            <div className="container px-4 px-lg-5">
                <a className="navbar-brand" href="/">Start Bootstrap</a>
                <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                        data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
                        aria-label="Toggle navigation">
                    Menu
                    <i className="fas fa-bars"></i>
                </button>
                <div className="collapse navbar-collapse" id="navbarResponsive">
                    <ul className="navbar-nav ms-auto py-4 py-lg-0">
                        {menuItems}
                    </ul>
                </div>
            </div>
        </nav>
    );

};

export default Nav;
