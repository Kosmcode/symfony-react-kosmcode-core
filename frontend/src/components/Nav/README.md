# Nav

<!-- STORY -->

## Introduction

Nav is an easy-to-use component.

## Usage

```javascript
import { Nav } from 'some-package-name';
```

## Example use

```javascript
const myPage = props => {
  return (
    <main>
      <Nav />
    </main>
  );
};
```

## Properties

- `className` - is the class name of the component

| propName  | propType | defaultValue | isRequired |
| --------- | -------- | ------------ | ---------- |
| className | string   | -            | -          |
