import {BaseRepository} from "./BaseRepository";

export default class MenuItemRepository extends BaseRepository {
    getMenuItems() {
        const QUERY = `
	    {
            items {
                edges {
                    node {
                        title
                        type
                        position
                        internalLinkPath
                        externalLinkUrl
                    }
                }
            }
        }
	    `;

        return this.makeGraphQLRequest(QUERY)
    }
}