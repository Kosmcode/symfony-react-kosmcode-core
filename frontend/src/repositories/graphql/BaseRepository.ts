import axios from 'axios';

export class BaseRepository {
    protected _graphQLEndpointUrl = '';

    constructor() {
        this.graphQLEndpointUrl = process.env.REACT_APP_BACKEND_GRAPHQL_ENDPOINT_URL ?? '';
    }

    get graphQLEndpointUrl(): string {
        return this._graphQLEndpointUrl;
    }

    set graphQLEndpointUrl(value: string) {
        this._graphQLEndpointUrl = value;
    }

    async makeGraphQLRequest(query: string, variables = {}) {
        let response = await axios({
            url: this.graphQLEndpointUrl,
            method: "POST",
            data: {
                query: query,
                variables: variables,
            },
        });
        return await response.data.data;
    }
}

