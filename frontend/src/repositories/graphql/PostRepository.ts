import {BaseRepository} from './BaseRepository';

export default class PostRepository extends BaseRepository {

    getPostBySlug(postSlug: string) {
        const POST_SLUG_QUERY = `
        query ($slug: String) {
            slugQueryPost(slug: $slug) {
                title
                headerDescription
                content
                metaTitle
                metaKeywords
                metaDescription
                updatedAt
                user {
                    username
                }
                headerImage {
                    base64
                }
            }
        }
        `;

        return this.makeGraphQLRequest(
            POST_SLUG_QUERY,
            {
                "slug": postSlug,
            }
        );
    }

    getPaginatedPost(page: number) {
        const PAGINATED_POSTS_QUERY = `
	    query ($page: Int) {
            posts(page: $page) {
                collection {
                    title
                    headerDescription
                    createdAt
                    slug
                    user {
                        username
                    }
                }
                paginationInfo {
                    hasNextPage
                }
            }
        }
	    `;

        return this.makeGraphQLRequest(
            PAGINATED_POSTS_QUERY,
            {
                'page': page,
            }
        )
    }
}