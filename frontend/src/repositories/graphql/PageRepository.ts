import {BaseRepository} from './BaseRepository';

export default class PageRepository extends BaseRepository {
    getPageBySlug(pageSlug: string) {
        const PAGE_SLUG_QUERY = `
	    query($slug: String) {
		    slugQueryPage(slug: $slug) {
			    content,
			    headerDescription,
			    metaTitle,
			    metaDescription,
			    metaKeywords,
			    headerImage {
                    base64
                }
		    }
	    }
	    `;

        return this.makeGraphQLRequest(
            PAGE_SLUG_QUERY,
            {
                "slug": pageSlug,
            }
        )
    }

}
