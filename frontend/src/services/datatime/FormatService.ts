export default class FormatService {
    formatTimestampToPostDate(timestamp = Number.MAX_SAFE_INTEGER): String {
        let date = new Date(timestamp);

        return date.toLocaleDateString('en-EN', {
            weekday: 'long',
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        });
    }
}