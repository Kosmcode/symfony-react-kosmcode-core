import {Link} from "react-router-dom";
import PostModel from "../../models/PostModel";
import FormatService from "../datatime/FormatService";

export default class PostService {

    dateTimeFormatService = new FormatService();

    renderPost(postModel = PostModel, index = Number.MAX_SAFE_INTEGER) {

        let divider;

        if (index !== 0) {
            divider = (<hr className="my-4"/>);
        }
        
        return (
            <div key={index}>
                {divider}

                <div className="post-preview">
                <Link to={'/post/' + postModel.slug}>
                    <h2 className="post-title">
                        {postModel.title}
                    </h2>
                    <h3 className="post-subtitle">
                        {postModel.headerDescription}
                    </h3>
                </Link>
                <p className="post-meta">
                    Posted by {postModel.user.username} on {this.dateTimeFormatService.formatTimestampToPostDate(parseInt(postModel.createdAt))}
                </p>
            </div>
        </div>
        );
    }


}