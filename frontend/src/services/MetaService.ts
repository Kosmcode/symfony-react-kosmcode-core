export default class MetaService {
    private _charset = 'UTF-8';
    private _publicUrl = '';
    private _canonicalLink = '';
    private _metaTitle = '';
    private _metaDescription = '';
    private _metaKeywords = '';

    constructor() {
        this.publicUrl = process.env.REACT_APP_PUBLIC_URL ?? 'http://localhost/';
    }

    get publicUrl(): string {
        return this._publicUrl;
    }

    set publicUrl(value: string) {
        this._publicUrl = value;
    }

    get charset(): string {
        return this._charset;
    }

    set charset(value: string) {
        this._charset = value;
    }

    get canonicalLink(): string {
        return this._canonicalLink;
    }

    set canonicalLink(value: string) {
        this._canonicalLink = value;
    }

    get metaTitle(): string {
        return this._metaTitle;
    }

    set metaTitle(value: string) {
        this._metaTitle = value;
    }

    get metaDescription(): string {
        return this._metaDescription;
    }

    set metaDescription(value: string) {
        this._metaDescription = value;
    }

    get metaKeywords(): string {
        return this._metaKeywords;
    }

    set metaKeywords(value: string) {
        this._metaKeywords = value;
    }

    appendToCanonicalLinkPage(value: string) {
        this._canonicalLink = this.publicUrl + this._canonicalLink + value;
    }

    getMetaObjectForDocumentMeta() {
        return {
            title: this.metaTitle,
            description: this.metaDescription,
            canonical: this.canonicalLink,
            meta: {
                charset: this.charset,
                name: {
                    keywords: this.metaKeywords,
                }
            }
        }
    }
}