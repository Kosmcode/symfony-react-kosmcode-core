import React from "react";
import {BrowserRouter, Route, Routes} from "react-router-dom";

import Layout from "./components/Layout";
import Home from "./components/Home";
import Page from "./components/Page";
import Posts from "./components/Posts";
import Post from "./components/Post";

function App() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Layout/>}>
                    <Route index element={<Home/>}/>
                    <Route path="/posts" element={<Posts />} />
                    <Route path="/posts/:page" element={<Posts />} />
                    <Route path="/post/:postSlug" element={<Post />} />
                    <Route path="/:pageSlug" element={<Page />} />
                </Route>
            </Routes>
        </BrowserRouter>
    );
}

export default App;
